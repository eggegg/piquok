package tw.nmlab.piquok;

public class Utils {
    static public double minmax(double x, double lowerbound, double upperbound) {
        return Math.min(upperbound, Math.max(x, lowerbound));
    }

    static public int minmax(int x, int lowerbound, int upperbound) {
        return Math.min(upperbound, Math.max(x, lowerbound));
    }

    static public double mod(double x, double y) {
        while (x < 0)
            x += y;
        while (x >= y)
            x -= y;
        return x;
    }

    static public double Arg(double x) {
        return mod(x, Math.PI * 2);
    }

    static public int randInt(int lowerbound, int upperbound) {
        if (lowerbound > upperbound)
            throw new IllegalArgumentException();
        return (int) (Math.random() * (upperbound - lowerbound + 1)) + lowerbound;
    }
}
