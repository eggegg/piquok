package tw.nmlab.piquok.lobby;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.game.Player.PlayerType;
import android.util.Log;
import android.view.MotionEvent;

public class ClientLobbyController extends LobbyController {
    private static final String TAG = "ClientLobbyController";

    public ClientLobbyController(LobbyActivity parent) {
        super(parent);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != MotionEvent.ACTION_DOWN)
            return false;
        double x = event.getX() / Global.widthScale, y = event.getY() / Global.heightScale;
        // Color selection
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (view.playerSelectBorder[i].contains((int) x, (int) y)) {
                if (playerName[i] == null && playerIsReady[selectedPlace] == false) {
                    Global.clientConnection.send(Global.ACTION_CHANGE_PLACE, new String[] {
                            Global.myName, String.valueOf(i) });
                    return true;
                }
            }
        }
        if (view.buttonStartBorder.contains((int) x, (int) y)) {
            Global.clientConnection
                    .send(Global.ACTION_CHANGE_READY, new String[] { Global.myName });
            return true;
        }

        if (view.buttonGraphicsQualityBorder.contains((int) x, (int) y)) {
            changeQuality();
            return true;
        }

        // Exit game
        if (view.buttonExitBorder.contains((int) x, (int) y)) {
            parent.flagInLobby = false;
            return true;
        }
        return false;
    }

    private int updateTime = 0;
    private static final int connectionTimeOut = 5000;

    @Override
    public void update() {
        updateTime += Global.frameInterval;
        if (!isConnected && updateTime > connectionTimeOut) {
            parent.flagInLobby = false;
            parent.exitLobbyReason = LobbyActivity.CONNECTION_TIME_OUT;
        }
    }

    public void exitLobby(int reason) {
        parent.flagInLobby = false;
        parent.exitLobbyReason = reason;
    }

    public void joinGame(String name, int place, boolean toast) {
        // All AI are just player in client view.
        types[place] = PlayerType.PLAYER.getId();
        playerName[place] = name;
        if (Global.myName.equals(name)) {
            selectedPlace = place;
        }
        if (toast) {
            parent.makeToast(String.format("%s had joined the game!", name));
        }
    }

    public void exitGame(String name, boolean toast) {
        int place = -1;
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (name.equals(playerName[i])) {
                place = i;
                break;
            }
        }
        if (place == -1) {
            Log.e(TAG, String.format("%s exited game but I don't know who it is...", name));
            return;
        }
        types[place] = PlayerType.NONE.getId();
        playerName[place] = null;
        if (place == selectedPlace) {
            Log.e(TAG, "I disconnected???");
        }
        if (toast) {
            parent.makeToast(String.format("%s had left the game!", name));
        }
    }

    public void changePlace(String name, int newPlace) {
        int place = -1;
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (name.equals(playerName[i])) {
                place = i;
                break;
            }
        }
        if (place == -1) {
            Log.e(TAG, String.format("%s changed place but I don't know who it is...", name));
            return;
        }
        types[place] = PlayerType.NONE.getId();
        playerName[place] = null;
        if (types[newPlace] != PlayerType.NONE.getId()) {
            Log.e(TAG, String.format("%s changed place to a occupied place...", name));
        }
        types[newPlace] = PlayerType.PLAYER.getId();
        playerName[newPlace] = name;
        if (selectedPlace == place) {
            selectedPlace = newPlace;
        }
    }

    public void changeIsReady(String name) {
        int place = -1;
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (name.equals(playerName[i])) {
                place = i;
                break;
            }
        }
        if (place == -1) {
            Log.e(TAG, String.format("%s changed ready state but I don't know who it is...", name));
            return;
        }
        playerIsReady[place] = !playerIsReady[place];
    }

    public void setIsReady(String name, boolean isReady) {
        int place = -1;
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (name.equals(playerName[i])) {
                place = i;
                break;
            }
        }
        if (place == -1) {
            Log.e(TAG, String.format("%s set ready state but I don't know who it is...", name));
            return;
        }
        playerIsReady[place] = isReady;
    }

    @Override
    public boolean readyState() {
        return !playerIsReady[selectedPlace];
    }
}
