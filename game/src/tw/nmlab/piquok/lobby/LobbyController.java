package tw.nmlab.piquok.lobby;

import tw.nmlab.piquok.Global;
import android.content.SharedPreferences;
import android.view.MotionEvent;

public abstract class LobbyController {
    protected LobbyActivity parent;
    protected LobbySurfaceView view;

    public boolean isConnected = false;

    public int[] types = new int[Global.maxPlayer];
    public String[] playerName = new String[Global.maxPlayer];
    public boolean[] playerIsReady = new boolean[Global.maxPlayer];
    // Should never be -1
    public int selectedPlace = -1;
    public int gameLength = 2;
    protected double[] baseGameLength = { 10.0, 30.0, 60.0, 120.0 };
    public int itemSpawnRate = 2;
    protected double[] baseItemSpawnRates = { 10000.0, 30.0, 15.0, 8.0 };
    public boolean isHost;
    public int graphicsQuality;
    SharedPreferences settings;

    protected void changeQuality() {
        graphicsQuality = 1 - graphicsQuality;
        settings.edit().putInt("graphicsQuality", graphicsQuality).commit();
    }

    public LobbyController(LobbyActivity parent) {
        this.parent = parent;
        settings = parent.getSharedPreferences("Preference", 0);
        graphicsQuality = settings.getInt("graphicsQuality", 0);
    }

    public void setView(LobbySurfaceView view) {
        this.view = view;
    }

    public abstract boolean readyState();

    public double getGameLength() {
        return baseGameLength[gameLength];
    }

    public double getItemSpawnRate() {
        return baseItemSpawnRates[itemSpawnRate];
    }

    public abstract boolean onTouchEvent(MotionEvent event);

    public abstract void update();
}
