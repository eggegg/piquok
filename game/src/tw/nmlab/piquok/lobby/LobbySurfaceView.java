package tw.nmlab.piquok.lobby;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.R;
import tw.nmlab.piquok.game.DrawUtils;
import tw.nmlab.piquok.game.GameController;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class LobbySurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = "LobbySurfaceView";

    private SurfaceHolder myholder;

    private LobbyActivity parent;

    private LobbyController lc;

    public void setLobbyController(LobbyController lc) {
        this.lc = lc;
    }

    // Standard scale matrix
    private Matrix scaleMatrix;

    // Text painting
    private int bigFontSize = 60;

    public Rect[] AISelectBorder = new Rect[Global.maxPlayer];
    public Rect[] playerSelectBorder = new Rect[Global.maxPlayer];

    public Rect buttonStartBorder;
    private String buttonStartText;
    private double buttonStartSize = 0.8;
    private double buttonStartX = 80, buttonStartY = 450;

    public Rect buttonExitBorder;
    private String buttonExitText = "Exit Game";
    public double buttonExitSize = 0.8;
    private double buttonExitX = 460, buttonExitY = 450;

    public Rect[] buttonTimeLengthBorder;
    private String[] buttonTimeLengthText = { "10", "30", "60", "120" };
    private double buttonTimeLengthSize = 0.6;
    private double[] buttonTimeLengthX = { 450, 530, 610, 685 }, buttonTimeLengthY = { 160, 160,
            160, 160 };

    public Rect[] buttonItemRateBorder;
    private String[] buttonItemRateText = { "none", "low", "normal", "high" };
    private double buttonItemRateSize = 0.4;
    private double[] buttonItemRateX = { 440, 525, 590, 705 }, buttonItemRateY = { 260, 260, 260,
            260 };

    public Rect buttonGraphicsQualityBorder;
    private String[] buttonGraphicsQualityText = { "high", "low" };
    private double buttonGraphicsQualitySize = 0.4;
    private double buttonGraphicsQualityX = 650, buttonGraphicsQualityY = 330;

    private Bitmap background;

    public LobbySurfaceView(LobbyActivity parent, boolean isHost) {
        super(parent);
        this.parent = parent;

        if (isHost) {
            buttonStartText = "Start Game!";
        } else {
            buttonStartText = "Ready!";
        }

        myholder = this.getHolder();
        myholder.addCallback(this);

        scaleMatrix = new Matrix();
        scaleMatrix.postScale((float) Global.widthScale, (float) Global.heightScale);

        for (int i = 0; i < Global.maxPlayer; i++) {
            int y = 80 + 80 * i;
            AISelectBorder[i] = new Rect(10, y + 10, 70, y + 70);
            playerSelectBorder[i] = new Rect(10, y + 10, 400, y + 70);
        }

        Paint paint = getTextSelectedPaint(buttonStartSize);
        buttonStartBorder = DrawUtils.getTextBoundBase(paint, buttonStartText, buttonStartX,
                buttonStartY);

        paint = getTextSelectedPaint(buttonExitSize);
        buttonExitBorder = DrawUtils.getTextBoundBase(paint, buttonExitText, buttonExitX,
                buttonExitY);

        paint = getTextSelectedPaint(buttonTimeLengthSize);
        buttonTimeLengthBorder = new Rect[buttonTimeLengthText.length];
        for (int i = 0; i < buttonTimeLengthText.length; i++) {
            buttonTimeLengthBorder[i] = DrawUtils.getTextBoundBase(paint, buttonTimeLengthText[i],
                    buttonTimeLengthX[i], buttonTimeLengthY[i]);
            Log.i(TAG, buttonTimeLengthBorder[i].toShortString());
        }

        paint = getTextSelectedPaint(buttonItemRateSize);
        buttonItemRateBorder = new Rect[buttonItemRateText.length];
        for (int i = 0; i < buttonItemRateText.length; i++) {
            buttonItemRateBorder[i] = DrawUtils.getTextBoundBase(paint, buttonItemRateText[i],
                    buttonItemRateX[i], buttonItemRateY[i]);
        }

        paint = getTextSelectedPaint(buttonGraphicsQualitySize);
        buttonGraphicsQualityBorder = DrawUtils.getTextBoundBaseCenter(paint,
                buttonGraphicsQualityText[0], buttonGraphicsQualityX, buttonGraphicsQualityY);
    }

    public void draw() {

        Canvas canvas = null;
        try {
            canvas = myholder.lockCanvas();
            if (!lc.isHost && !lc.isConnected) {
                canvas.drawColor(Color.BLACK);
                Paint paint = getTextPaint(1.2);
                paint.setARGB(255, 196, 196, 196);
                DrawUtils.drawTextCenterScreen(canvas, "Connecting ...", paint);
            } else {
                canvas.drawColor(Color.WHITE);
                canvas.drawBitmap(background, scaleMatrix, null);

                Paint paint = getTextPaint(0.8);
                Paint borderPaint = getBorderPaint();
                DrawUtils.drawTextCenterAlign(canvas, "Game Options", paint, 20);

                // Color selection.
                for (int i = 0; i < Global.maxPlayer; i++) {
                    int y = 80 + 80 * i;
                    // TODO change to brush picture
                    Rect rect = new Rect(20, y + 20, 60, y + 60);
                    paint.setColor(GameController.getPlayerColorById(i));
                    DrawUtils.drawRect(canvas, rect, paint);
                    paint.setARGB(255, 32, 32, 32);
                    DrawUtils.drawRect(canvas, playerSelectBorder[i], borderPaint);
                    if (lc.playerName[i] != null) {
                        Paint namePaint = getTextPaint(0.6);
                        DrawUtils
                                .drawTextLeftAlign(canvas, lc.playerName[i], namePaint, 80, y + 25);
                        if (lc.playerIsReady[i]) {
                            Paint readyPaint = getTextPaint(0.25);
                            readyPaint.setARGB(255, 55, 196, 255);
                            DrawUtils.drawTextRightAlignBase(canvas, "ready", readyPaint, 395,
                                    y + 30);
                        }
                    } else {
                        Paint namePaint = getTextPaint(0.3);
                        DrawUtils.drawTextLeftAlign(canvas, "Click to select", namePaint, 80,
                                y + 35);
                    }
                }

                // Game Length selection
                Paint optionNamePaint = getTextPaint(0.4);
                DrawUtils.drawTextCenterAlign(canvas, "Time", optionNamePaint, 100, 380, 800);
                for (int i = 0; i < buttonTimeLengthText.length; i++) {
                    Paint optionPaint;
                    if (lc.gameLength == i) {
                        // Selected
                        optionPaint = getTextSelectedPaint(buttonTimeLengthSize);
                        if (!lc.isHost) {
                            optionPaint.setARGB(255, 239, 159, 12);
                        }
                    } else {
                        optionPaint = getTextUnavailablePaint(buttonTimeLengthSize);
                    }
                    DrawUtils.drawTextLeftAlignBase(canvas, buttonTimeLengthText[i], optionPaint,
                            buttonTimeLengthX[i], buttonTimeLengthY[i]);
                }

                // Item Rate selection
                DrawUtils.drawTextCenterAlign(canvas, "Item", optionNamePaint, 200, 380, 800);
                for (int i = 0; i < buttonItemRateText.length; i++) {
                    Paint optionPaint;
                    if (lc.itemSpawnRate == i) {
                        // Selected
                        optionPaint = getTextSelectedPaint(buttonItemRateSize);
                        if (!lc.isHost) {
                            optionPaint.setARGB(255, 239, 159, 12);
                        }
                    } else {
                        optionPaint = getTextUnavailablePaint(buttonItemRateSize);
                    }
                    DrawUtils.drawTextLeftAlignBase(canvas, buttonItemRateText[i], optionPaint,
                            buttonItemRateX[i], buttonItemRateY[i]);
                }

                // Graphics Quality
                DrawUtils.drawTextLeftAlignBase(canvas, "Graphics:", optionNamePaint, 500,
                        buttonGraphicsQualityY);
                Paint optionPaint;
                optionPaint = getTextSelectedPaint(buttonGraphicsQualitySize);
                DrawUtils.drawTextCenterAlignBase(canvas,
                        buttonGraphicsQualityText[lc.graphicsQuality], optionPaint,
                        buttonGraphicsQualityY, 600, 700);

                if (lc.isHost) {
                    Paint namePaint = getTextPaint(0.3);
                    namePaint.setARGB(255, 128, 128, 128);
                    DrawUtils.drawTextRightAlignBase(canvas, "Click color to add/remove AI",
                            namePaint, 780, 360);
                }

                // Start game button
                if (!lc.readyState()) {
                    Paint buttonPaint = getTextUnavailablePaint(buttonStartSize);
                    DrawUtils.drawTextLeftAlignBase(canvas, buttonStartText, buttonPaint,
                            buttonStartX, buttonStartY);
                } else {
                    Paint buttonPaint1 = getTextSelectedPaint(buttonStartSize);
                    DrawUtils.drawTextLeftAlignBase(canvas, buttonStartText, buttonPaint1,
                            buttonStartX, buttonStartY);
                    Paint buttonPaint2 = getTextStrokePaint(buttonStartSize);
                    DrawUtils.drawTextLeftAlignBase(canvas, buttonStartText, buttonPaint2,
                            buttonStartX, buttonStartY);
                }

                // Exit game button
                Paint buttonPaint1 = getTextSelectedPaint(buttonExitSize);
                DrawUtils.drawTextLeftAlignBase(canvas, buttonExitText, buttonPaint1, buttonExitX,
                        buttonExitY);
                Paint buttonPaint2 = getTextStrokePaint(buttonExitSize);
                DrawUtils.drawTextLeftAlignBase(canvas, buttonExitText, buttonPaint2, buttonExitX,
                        buttonExitY);
            }
        } catch (Exception ex) {
        } finally {
            if (canvas != null)
                myholder.unlockCanvasAndPost(canvas);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return lc.onTouchEvent(event);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // Nothing to do.
    }

    private Thread gameThread;

    private void initializeBitmap() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        // Bitmaps
        background = BitmapFactory.decodeResource(getResources(), R.drawable.background, options);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        initializeBitmap();
        gameThread = new Thread(parent);
        gameThread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // Nothing to do.
    }

    private Paint getBorderPaint() {
        Paint paint = new Paint();
        paint.setARGB(255, 32, 32, 32);
        paint.setStyle(Paint.Style.STROKE);
        return paint;
    }

    private Paint getTextPaint(double scale) {
        Paint paint = new Paint();
        paint.setARGB(255, 32, 32, 32);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize((int) (bigFontSize * Global.heightScale * scale));
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setAntiAlias(true);
        return paint;
    }

    private Paint getTextSelectedPaint(double scale) {
        Paint paint = new Paint();
        paint.setARGB(255, 244, 169, 28);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize((int) (bigFontSize * Global.heightScale * scale));
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setAntiAlias(true);
        return paint;
    }

    private Paint getTextStrokePaint(double scale) {
        Paint strokePaint = new Paint();
        strokePaint.setARGB(255, 32, 32, 32);
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setTextSize((int) (bigFontSize * Global.heightScale * scale));
        strokePaint.setTypeface(Typeface.DEFAULT_BOLD);
        strokePaint.setAntiAlias(true);
        strokePaint.setStrokeWidth(1);
        return strokePaint;
    }

    private Paint getTextUnavailablePaint(double scale) {
        Paint paint = new Paint();
        paint.setARGB(255, 64, 64, 64);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize((int) (bigFontSize * Global.heightScale * scale));
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setAntiAlias(true);
        return paint;
    }

}
