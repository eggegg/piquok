package tw.nmlab.piquok.lobby;

import java.io.IOException;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.R;
import tw.nmlab.piquok.connection.ClientConnection;
import tw.nmlab.piquok.connection.ServerConnection;
import tw.nmlab.piquok.game.GameActivity;
import tw.nmlab.piquok.game.Player.PlayerType;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class LobbyActivity extends Activity implements Runnable {
    private static String TAG = "Lobby";

    private LobbySurfaceView mySufView;
    private LobbyController lc;
    public boolean flagInLobby;
    private boolean isSinglePlayer;
    public int exitLobbyReason = -1;
    public static final int CONNECTION_TIME_OUT = 0;
    public static final int GAME_START = 1;
    public static final int FULL_GAME = 2;
    public static final int INVALID_NAME = 3;
    public static final int DISCONNECTED = 4;
    public static final int GAME_ALREADY_START = 5;
    private MediaPlayer musicPlayer;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // For displaying toast.
        HandlerThread uiThread = new HandlerThread("UIHandler");
        uiThread.start();
        uiHandler = new UIHandler(uiThread.getLooper());

        musicPlayer = MediaPlayer.create(this, R.raw.selection);
        musicPlayer.setLooping(true);
        musicPlayer.start();

        Bundle bundle = getIntent().getExtras();
        boolean isHost = bundle.getBoolean("isHost");
        mySufView = new LobbySurfaceView(this, isHost);
        if (isHost) {
            lc = new ServerLobbyController(this);
            lc.selectedPlace = 0;
            lc.types[0] = PlayerType.PLAYER.getId();
            lc.playerName[0] = Global.myName;
        } else {
            lc = new ClientLobbyController(this);
        }
        mySufView.setLobbyController(lc);
        lc.setView(mySufView);
        lc.isHost = isHost;

        flagInLobby = true;

        isSinglePlayer = bundle.getBoolean("isSinglePlayer");
        if (!isSinglePlayer) {
            if (isHost) {
                makeToast("Please open hotspot manually.");
                try {
                    Global.serverConnection = new ServerConnection(Global.serverPort);
                } catch (IOException e) {
                    Log.e(TAG, "Cannot create server connection");
                }
                Global.serverConnection.registerLobbyController((ServerLobbyController) lc);
                Global.serverConnection.start();
            } else {
                WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
                DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();

                int ipadd = dhcpInfo.gateway;
                String ip = (ipadd & 0xFF) + "." + (ipadd >> 8 & 0xFF) + "." + (ipadd >> 16 & 0xFF)
                        + "." + (ipadd >> 24 & 0xFF);

                try {
                    Global.clientConnection = new ClientConnection(ip, Global.serverPort);
                    Global.clientConnection.registerLobbyController((ClientLobbyController) lc);
                    Global.clientConnection.start();
                    Global.clientConnection.send(Global.ACTION_JOIN_GAME,
                            new String[] { Global.myName });
                } catch (IOException e) {
                    e.printStackTrace();
                    exitLobbyReason = CONNECTION_TIME_OUT;
                    flagInLobby = false;
                }
            }
        }
        setContentView(mySufView);
    }

    @Override
    public void onPause() {
        super.onPause();
        musicPlayer.stop();
    }

    @Override
    public void run() {
        Global.loadedClient = 0;
        while (flagInLobby) {
            long c1 = System.currentTimeMillis();
            lc.update();
            mySufView.draw();
            long c2 = System.currentTimeMillis();
            try {
                Thread.sleep(Math.max(1, Global.frameInterval - (c2 - c1)));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (exitLobbyReason == GAME_START) {
            Intent intent = new Intent(LobbyActivity.this, GameActivity.class);
            Bundle bundle = new Bundle();
            bundle.putDouble("itemSpawnRate", lc.getItemSpawnRate());
            bundle.putDouble("gameLength", lc.getGameLength());
            for (int i = 0; i < 4; i++) {
                bundle.putInt(String.format("playerType%d", i), lc.types[i]);
                bundle.putString(String.format("playerName%d", i), lc.playerName[i]);
            }
            bundle.putInt("playerId", lc.selectedPlace);
            bundle.putBoolean("isHost", lc.isHost);
            bundle.putBoolean("isSinglePlayer", isSinglePlayer);
            bundle.putInt("graphicsQuality", lc.graphicsQuality);
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            if (exitLobbyReason == CONNECTION_TIME_OUT) {
                makeToast("Connection timed out.");
            } else if (exitLobbyReason == FULL_GAME) {
                makeToast("The game is full.");
            } else if (exitLobbyReason == INVALID_NAME) {
                makeToast("Your name is invalid.");
            } else if (exitLobbyReason == DISCONNECTED) {
                makeToast("Disconnected.");
            } else if (exitLobbyReason == GAME_ALREADY_START) {
                makeToast("The game had already begun.");
            }
            if (lc.isHost) {
                if (Global.serverConnection != null) {
                    Global.serverConnection.end();
                    Global.serverConnection = null;
                }
            } else {
                if (Global.clientConnection != null) {
                    Global.clientConnection.end();
                    Global.clientConnection = null;
                } else {
                    Log.e(TAG, "Client doesn't have a connection.");
                }
            }
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        flagInLobby = false;
    }

    private final class UIHandler extends Handler {

        public UIHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            Context context = getApplicationContext();
            Toast t = Toast.makeText(context, (String) msg.obj, Toast.LENGTH_SHORT);
            t.show();
        }
    }

    private UIHandler uiHandler;

    public void makeToast(String message) {
        Message msg = uiHandler.obtainMessage();
        msg.obj = message;
        uiHandler.sendMessage(msg);
    }

}
