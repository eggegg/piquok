package tw.nmlab.piquok.lobby;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.game.Player;
import tw.nmlab.piquok.game.Player.PlayerType;
import android.util.Log;
import android.view.MotionEvent;

public class ServerLobbyController extends LobbyController {
    private static final String TAG = "ServerLobbyController";

    // Not used...
    public int[] connectionIdMap = new int[Global.maxPlayer];

    public ServerLobbyController(LobbyActivity parent) {
        super(parent);
        this.isConnected = true;
        for (int i = 0; i < Global.maxPlayer; i++)
            connectionIdMap[i] = -1;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != MotionEvent.ACTION_DOWN)
            return false;
        double x = event.getX() / Global.widthScale, y = event.getY() / Global.heightScale;
        // Color selection
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (view.AISelectBorder[i].contains((int) x, (int) y)) {
                if (playerName[i] == null) {
                    joinGame(Player.computerName[i], i, -1, false, true);
                    changeIsReady(Player.computerName[i]);
                    return true;
                } else if (types[i] == PlayerType.AI.getId()) {
                    exitGame(Player.computerName[i], false);
                    return true;
                }
            }
            if (view.playerSelectBorder[i].contains((int) x, (int) y)) {
                if (playerName[i] == null) {
                    changePlace(Global.myName, i);
                    return true;
                }
            }
        }

        // Game length selection
        for (int i = 0; i < view.buttonTimeLengthBorder.length; i++) {
            if (view.buttonTimeLengthBorder[i].contains((int) x, (int) y)) {
                gameLength = i;
                if (Global.serverConnection != null)
                    Global.serverConnection.broadcast(Global.RESPONSE_GAME_INFO, new String[] {
                            "gameLength", String.valueOf(gameLength) });
                return true;
            }
        }

        // Item Rate selection
        for (int i = 0; i < view.buttonItemRateBorder.length; i++) {
            if (view.buttonItemRateBorder[i].contains((int) x, (int) y)) {
                itemSpawnRate = i;
                if (Global.serverConnection != null)
                    Global.serverConnection.broadcast(Global.RESPONSE_GAME_INFO, new String[] {
                            "itemSpawnRate", String.valueOf(itemSpawnRate) });
                return true;
            }
        }

        if (view.buttonGraphicsQualityBorder.contains((int) x, (int) y)) {
            changeQuality();
            return true;
        }

        // Start game
        if (readyState() && view.buttonStartBorder.contains((int) x, (int) y)) {
            if (Global.serverConnection != null)
                Global.serverConnection.broadcast(Global.RESPONSE_GAME_START, null);
            parent.flagInLobby = false;
            parent.exitLobbyReason = LobbyActivity.GAME_START;
            return true;
        }

        // Exit game
        if (view.buttonExitBorder.contains((int) x, (int) y)) {
            parent.flagInLobby = false;
            return true;
        }
        return false;
    }

    public void changeIsReady(String name) {
        int place = -1;
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (name.equals(playerName[i])) {
                place = i;
                break;
            }
        }
        if (place == -1) {
            Log.e(TAG, String.format("%s changed ready state but I don't know who it is...", name));
            return;
        }
        playerIsReady[place] = !playerIsReady[place];
        if (Global.serverConnection != null)
            Global.serverConnection.broadcast(Global.RESPONSE_READY_CHANGE, new String[] { name });
    }

    public int tryToJoinGame(int connectionId, String name) {
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (Player.computerName[i].equals(name)) {
                return -1;
            }
        }
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (playerName[i] != null && playerName[i].equals(name)) {
                return -1;
            }
        }
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (playerName[i] == null) {
                joinGame(name, i, connectionId, true, false);
                return i;
            }
        }
        return -2;
    }

    public void joinGame(String name, int place, int connectionId, boolean toast, boolean isAI) {
        if (isAI) {
            types[place] = PlayerType.AI.getId();
        } else {
            types[place] = PlayerType.PLAYER.getId();
        }
        playerName[place] = name;
        connectionIdMap[place] = connectionId;
        if (Global.myName.equals(name)) {
            selectedPlace = place;
        }
        if (toast) {
            parent.makeToast(String.format("%s had joined the game!", name));
        }
        if (Global.serverConnection != null)
            Global.serverConnection.broadcast(Global.RESPONSE_GAME_JOINED, new String[] { name,
                    String.valueOf(place) });
    }

    public void exitGame(String name, boolean toast) {
        int place = -1;
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (name.equals(playerName[i])) {
                place = i;
                break;
            }
        }
        if (place == -1) {
            Log.e(TAG, String.format("%s exited game but I don't know who it is...", name));
            return;
        }
        types[place] = PlayerType.NONE.getId();
        playerName[place] = null;
        connectionIdMap[place] = -1;
        playerIsReady[place] = false;
        if (place == selectedPlace) {
            Log.e(TAG, "I disconnected???");
        }
        if (toast) {
            parent.makeToast(String.format("%s had left the game!", name));
        }
        if (Global.serverConnection != null)
            Global.serverConnection.broadcast(Global.RESPONSE_GAME_EXITED, new String[] { name });
    }

    public void changePlace(String name, int newPlace) {
        int place = -1;
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (name.equals(playerName[i])) {
                place = i;
                break;
            }
        }
        if (place == -1) {
            Log.e(TAG, String.format("%s changed place but I don't know who it is...", name));
            return;
        }
        if (playerIsReady[place]) {
            Log.e(TAG, String.format("%s is ready and shouldn't be changing place...", name));
            return;
        }
        types[place] = PlayerType.NONE.getId();
        playerName[place] = null;
        if (types[newPlace] != PlayerType.NONE.getId()) {
            Log.e(TAG, String.format("%s changed place to a occupied place...", name));
        }
        types[newPlace] = PlayerType.PLAYER.getId();
        playerName[newPlace] = name;
        connectionIdMap[newPlace] = connectionIdMap[place];
        connectionIdMap[place] = -1;
        if (selectedPlace == place) {
            selectedPlace = newPlace;
        }
        if (Global.serverConnection != null)
            Global.serverConnection.broadcast(Global.RESPONSE_PLACE_CHANGE, new String[] { name,
                    String.valueOf(newPlace) });
    }

    @Override
    public void update() {

    }

    @Override
    public boolean readyState() {
        // In fact, start game state.
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (i != selectedPlace && types[i] != PlayerType.NONE.getId() && !playerIsReady[i])
                return false;
        }
        return true;
    }
}
