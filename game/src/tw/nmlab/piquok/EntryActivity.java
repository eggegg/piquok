package tw.nmlab.piquok;

import tw.nmlab.piquok.lobby.LobbyActivity;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.method.SingleLineTransformationMethod;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class EntryActivity extends Activity implements Runnable {
    private EntrySurfaceView myView;
    private boolean inEntry = true;
    private static int updateTime = 0;
    private MediaPlayer musicPlayer;

    public void queryName() {
        final Runnable r = new Runnable() {

            @Override
            public void run() {
                final SharedPreferences settings = getSharedPreferences("Preference", 0);
                final AlertDialog.Builder alert = new AlertDialog.Builder(EntryActivity.this);
                final EditText input = new EditText(EntryActivity.this);
                input.setFilters(new InputFilter[] { new InputFilter.LengthFilter(12) });
                input.setTransformationMethod(new SingleLineTransformationMethod());
                input.setText(Global.myName);
                input.setSelectAllOnFocus(true);
                alert.setView(input);
                alert.setTitle("Please input your name:");
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Global.myName = input.getText().toString().replaceAll(" ", "");
                        settings.edit().putString("name", Global.myName).commit();
                    }
                });
                alert.show();
            }

        };
        myView.post(r);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // setting display metric
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Global.initialize(dm);
    }

    @Override
    protected void onResume() {
        super.onResume();
        inEntry = true;
        myView = new EntrySurfaceView(this, updateTime);

        // Load name.
        SharedPreferences settings = getSharedPreferences("Preference", 0);
        String name = settings.getString("name", "");
        Global.myName = name;

        // setting entry layout
        setContentView(myView);

        musicPlayer = MediaPlayer.create(this, R.raw.opening);
        musicPlayer.setLooping(true);
        musicPlayer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        musicPlayer.stop();
    }

    @Override
    public void run() {
        while (inEntry) {
            myView.draw();
            long c1 = System.currentTimeMillis();
            myView.draw();
            long c2 = System.currentTimeMillis();
            try {
                Thread.sleep(Math.max(1, Global.frameInterval - (c2 - c1)));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        updateTime = myView.getUpdateTime();
    }

    public void onSingleClick(View view) {
        if (Global.myName.length() == 0) {
            Toast.makeText(view.getContext(), "Please enter your name.", Toast.LENGTH_LONG).show();
            return;
        }
        inEntry = false;
        Intent intent = new Intent(EntryActivity.this, LobbyActivity.class);
        intent.putExtra("isHost", true);
        intent.putExtra("isSinglePlayer", true);
        startActivity(intent);
    }

    public void onJoinClick(View view) {
        if (Global.myName.length() == 0) {
            Toast.makeText(view.getContext(), "Please enter your name.", Toast.LENGTH_LONG).show();
            return;
        }
        inEntry = false;
        Intent intent = new Intent(EntryActivity.this, LobbyActivity.class);
        intent.putExtra("isHost", false);
        intent.putExtra("isSinglePlayer", false);
        startActivity(intent);
    }

    public void onHostClick(View view) {
        if (Global.myName.length() == 0) {
            Toast.makeText(view.getContext(), "Please enter your name.", Toast.LENGTH_LONG).show();
            return;
        }
        inEntry = false;
        Intent intent = new Intent(EntryActivity.this, LobbyActivity.class);
        intent.putExtra("isHost", true);
        intent.putExtra("isSinglePlayer", false);
        startActivity(intent);
    }
}
