package tw.nmlab.piquok.parser;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.StringReader;

import android.util.Log;

import tw.nmlab.piquok.Global;

public abstract class Parser extends Thread {
    private final static String TAG = "Parser";

    private PipedInputStream istream;
    StringBuilder sb = new StringBuilder();

    public Parser(PipedOutputStream ostream) {
        try {
            istream = new PipedInputStream(ostream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    abstract public void sendItOut();

    abstract protected void disconnected();

    @Override
    public void run() {
        Log.i(TAG, "Parser started");
        int ch = 0;
        while (true) {
            try {
                ch = istream.read();
            } catch (IOException e) {
                disconnected();
                return;
            }
            if (ch == -1) {
                disconnected();
                return;
            }
            if (ch == Global.stringSeparator) {
                sendItOut();
                sb.setLength(0);
            } else {
                sb.append((char) ch);
            }
        }
    }

    protected String readString(StringReader rd) {
        StringBuilder s = new StringBuilder();
        while (true) {
            int ch = 0;
            try {
                ch = rd.read();
            } catch (IOException e) {
            }
            if (ch < 0 || ch == ' ' || ch == '\n')
                break;
            s.append((char) ch);
        }
        return s.toString();
    }

    protected int readInt(StringReader rd) {
        return Integer.valueOf(readString(rd));
    }

    protected double readDouble(StringReader rd) {
        return Double.valueOf(readString(rd));
    }
    protected boolean readBoolean(StringReader rd) {
        return Boolean.valueOf(readString(rd));
    }

}
