package tw.nmlab.piquok.parser;

import java.io.PipedOutputStream;
import java.io.StringReader;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.connection.ServerConnection;
import tw.nmlab.piquok.game.Player.PlayerType;
import tw.nmlab.piquok.game.ServerGameController;
import tw.nmlab.piquok.lobby.ServerLobbyController;
import android.util.Log;

public class ServerParser extends Parser {
    private static final String TAG = "ServerParser";

    private ServerGameController gameController = null;
    private ServerLobbyController lobbyController = null;
    private ServerConnection parent = null;
    private String name = null;

    private int connectionId;

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }

    public int getConnectionId() {
        return connectionId;
    }

    public ServerParser(PipedOutputStream ostream, ServerConnection parent) {
        super(ostream);
        this.parent = parent;
    }

    public void registerLobbyController(ServerLobbyController control) {
        lobbyController = control;
    }

    public void registerGameController(ServerGameController control) {
        gameController = control;
    }

    @Override
    public void sendItOut() {
        StringReader rd = new StringReader(sb.toString());
        int mtype = readInt(rd);
        String name;
        int value;
        switch (mtype) {
        // Testing
        case Global.ACTION_TEST:
            String str = readString(rd);
            String str2 = readString(rd);
            Log.i(TAG, str + " " + str2);
            break;
        // In lobby
        case Global.ACTION_JOIN_GAME:
            if (isInLobby()) {
                name = readString(rd);
                Log.d("sendItOut", name);
                int place = lobbyController.tryToJoinGame(connectionId, name);
                Log.d("sendItOut", String.valueOf(place));
                if (place >= 0) {
                    Global.serverConnection.send(connectionId, Global.RESPONSE_CONNECTED, null);
                    for (int i = 0; i < Global.maxPlayer; i++) {
                        if (i != place && lobbyController.types[i] != PlayerType.NONE.getId()) {
                            Global.serverConnection.send(
                                    connectionId,
                                    Global.RESPONSE_GAME_PLAYER,
                                    new String[] { lobbyController.playerName[i],
                                            String.valueOf(i),
                                            String.valueOf(lobbyController.playerIsReady[i]) });
                        }
                    }
                    Global.serverConnection
                            .send(connectionId, Global.RESPONSE_GAME_INFO, new String[] {
                                    "gameLength", String.valueOf(lobbyController.gameLength) });
                    Global.serverConnection.send(
                            connectionId,
                            Global.RESPONSE_GAME_INFO,
                            new String[] { "itemSpawnRate",
                                    String.valueOf(lobbyController.itemSpawnRate) });
                    this.name = name;
                } else if (place == -1) { // Invalid name
                    Global.serverConnection.send(connectionId, Global.RESPONSE_INVALID_NAME, null);
                } else { // Full game.
                    Global.serverConnection.send(connectionId, Global.RESPONSE_FULL_GAME, null);
                }
            } else {
                Log.e(TAG, "receiving game join action when not in lobby...");
            }
            break;
        case Global.ACTION_CHANGE_PLACE:
            if (isInLobby()) {
                name = readString(rd);
                value = readInt(rd);
                lobbyController.changePlace(name, value);
            } else {
                Log.e(TAG, "receiving place change action when not in lobby...");
            }
            break;
        case Global.ACTION_CHANGE_READY:
            if (isInLobby()) {
                name = readString(rd);
                lobbyController.changeIsReady(name);
            } else {
                Log.e(TAG, "receiving ready action when not in lobby...");
            }
            break;
        // In between
        case Global.RESPONSE_GAME_LOADED:
            Global.loadedClient++;
            break;
        // In game
        case Global.ACTION_CHANGE_MODE:
            if (isInGame()) {
                value = readInt(rd);
                gameController.changeMode(value);
            } else {
                Log.e(TAG, "receiving mode change action when not in lobby...");
            }
            break;
        case Global.ACTION_CHANGE_DIRECTION:
            if (isInGame()) {
                value = readInt(rd);
                int dir = readInt(rd);
                gameController.setDirection(value, dir);
            } else {
                Log.e(TAG, "receiving mode change action when not in lobby...");
            }
            break;
        }
    }

    private boolean isInGame() {
        return gameController != null;
    }

    private boolean isInLobby() {
        return gameController == null && lobbyController != null;
    }

    @Override
    protected void disconnected() {
        parent.disconnect(this);
        Log.i(TAG, "disconnected()");
        if (this.name != null) {
            if (gameController != null) {
                gameController.playerExitGame(name);
            } else if (lobbyController != null) {
                lobbyController.exitGame(this.name, true);
            }
        }
    }

}
