package tw.nmlab.piquok.parser;

import java.io.PipedOutputStream;
import java.io.StringReader;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.game.ClientGameController;
import tw.nmlab.piquok.lobby.ClientLobbyController;
import tw.nmlab.piquok.lobby.LobbyActivity;
import android.util.Log;

public class ClientParser extends Parser {
    private static final String TAG = "ClientParser";

    private ClientGameController gameController = null;
    private ClientLobbyController lobbyController = null;

    public ClientParser(PipedOutputStream ostream) {
        super(ostream);
    }

    public void registerLobbyController(ClientLobbyController control) {
        lobbyController = control;
    }

    public void registerGameController(ClientGameController control) {
        gameController = control;
    }

    @Override
    public void sendItOut() {
        StringReader rd = new StringReader(sb.toString());
        String name;
        int value;
        int mtype = readInt(rd);
        switch (mtype) {
        // Testing
        case Global.ACTION_TEST:
            String str = readString(rd);
            String str2 = readString(rd);
            Log.i(TAG, str + " " + str2);
            break;
        // In lobby
        case Global.RESPONSE_FULL_GAME:
            if (isInLobby()) {
                lobbyController.exitLobby(LobbyActivity.FULL_GAME);
            } else {
                Log.e(TAG, "Get full game response when not in lobby.");
            }
            break;
        case Global.RESPONSE_INVALID_NAME:
            if (isInLobby()) {
                lobbyController.exitLobby(LobbyActivity.INVALID_NAME);
            } else {
                Log.e(TAG, "Get invalid name response when not in lobby.");
            }
            break;
        case Global.RESPONSE_CONNECTED:
            if (isInLobby()) {
                lobbyController.isConnected = true;
            } else {
                Log.e(TAG, "Get connected response when not in lobby.");
            }
            break;
        case Global.RESPONSE_GAME_JOINED:
            if (isInLobby()) {
                name = readString(rd);
                value = readInt(rd);
                lobbyController.joinGame(name, value, true);
            } else {
                Log.e(TAG, "Get game joined response when not in lobby.");
            }
            break;
        case Global.RESPONSE_GAME_PLAYER:
            if (isInLobby()) {
                name = readString(rd);
                value = readInt(rd);
                boolean isReady = readBoolean(rd);
                lobbyController.joinGame(name, value, false);
                lobbyController.setIsReady(name, isReady);
            } else {
                Log.e(TAG, "Get player info response when not in lobby.");
            }
            break;
        case Global.RESPONSE_GAME_INFO:
            if (isInLobby()) {
                name = readString(rd);
                value = readInt(rd);
                if (name.equals("gameLength")) {
                    lobbyController.gameLength = value;
                } else if (name.equals("itemSpawnRate")) {
                    lobbyController.itemSpawnRate = value;
                }
            } else {
                Log.e(TAG, "Get game info response when not in lobby.");
            }
            break;
        case Global.RESPONSE_GAME_EXITED:
            if (isInLobby()) {
                name = readString(rd);
                lobbyController.exitGame(name, true);
            }
            break;
        case Global.RESPONSE_PLACE_CHANGE:
            if (isInLobby()) {
                name = readString(rd);
                value = readInt(rd);
                lobbyController.changePlace(name, value);
            } else {
                Log.e(TAG, "Get place change response when not in lobby.");
            }
            break;
        case Global.RESPONSE_READY_CHANGE:
            if (isInLobby()) {
                name = readString(rd);
                lobbyController.changeIsReady(name);
            } else {
                Log.e(TAG, "Get ready change response when not in lobby.");
            }
            break;
        case Global.RESPONSE_GAME_START:
            if (isInLobby()) {
                lobbyController.exitLobby(LobbyActivity.GAME_START);
            } else {
                Log.e(TAG, "Get game start response when not in lobby.");
            }
            break;
        // In game
        case Global.RESPONSE_GAME_LOADED:
            if (isInGame()) {
                gameController.gameStarted = true;
            } else {
                Log.e(TAG, "Get game loaded response when not in game.");
            }
            break;
        case Global.RESPONSE_PLAYER_INFO:
            if (isInGame()) {
                int id = readInt(rd);
                double xPos = readDouble(rd);
                double yPos = readDouble(rd);
                int mode = readInt(rd);
                double faceAngle = readDouble(rd);
                int effect = readInt(rd);
                int effectTime = readInt(rd);
                gameController.setPlayerInfo(id, xPos, yPos, mode, faceAngle, effect, effectTime);
            } else {
                Log.e(TAG, "Get player info response when not in game.");
            }
            break;
        case Global.RESPONSE_PLAYER_START_FLY:
            if (isInGame()) {
                int id = readInt(rd);
                gameController.setStartFly(id);
            } else {
                Log.e(TAG, "Get player start fly response when not in game.");
            }
            break;
        case Global.RESPONSE_ITEM_INFO:
            if (isInGame()) {
                int itemId = readInt(rd);
                int xPos = readInt(rd);
                int yPos = readInt(rd);
                gameController.setItemInfo(itemId, xPos, yPos);
            } else {
                Log.e(TAG, "Get item info response when not in game.");
            }
            break;
        case Global.RESPONSE_PLAYER_GOT_ITEM:
            if (isInGame()) {
                int playerId = readInt(rd);
                int itemId = readInt(rd);
                gameController.playerGotItem(playerId, itemId);
            } else {
                Log.e(TAG, "Get player got item response when not in game.");
            }
            break;
        case Global.RESPONSE_GAME_ENDED:
            if (isInGame()) {
                gameController.gameEnded = true;
                int[] count = new int[Global.maxPlayer];
                for (int i = 0; i < Global.maxPlayer; i++) {
                    count[i] = readInt(rd);
                }
                gameController.setColors(count);
            } else {
                Log.e(TAG, "Get end game response when not in game.");
            }
            break;
        case Global.RESPONSE_GAME_TIME:
            if (isInGame()) {
                value = readInt(rd);
                gameController.setUpdateNumber(value);
            } else {
                Log.e(TAG, "Get game time response when not in game.");
            }
            break;
        case Global.RESPONSE_PLAYER_DISCONNECTED:
            if (isInGame()) {
                value = readInt(rd);
                gameController.playerDisconnected(value);
            } else {
                Log.e(TAG, "Get player disconnected response when not in game.");
            }
            break;
        case Global.RESPONSE_GAME_ALREADY_START:
            if (isInLobby()) {
                lobbyController.exitLobby(LobbyActivity.GAME_ALREADY_START);
            } else {
                Log.e(TAG, "Get game already start response when not in lobby.");
            }
            break;
        case Global.RESPONSE_PLAYER_BOUNCE:
            if (isInGame()) {
                gameController.playBounceSound();
            } else {
                Log.e(TAG, "Get bounce response when not in game.");
            }
            break;
        }
    }

    private boolean isInGame() {
        return gameController != null;
    }

    private boolean isInLobby() {
        return gameController == null && lobbyController != null;
    }

    @Override
    protected void disconnected() {
        if (isInGame()) {
            gameController.disconnected();
        } else if (isInLobby()) {
            lobbyController.exitLobby(LobbyActivity.DISCONNECTED);
        }
    }
}
