package tw.nmlab.piquok;

import tw.nmlab.piquok.game.DrawUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class EntrySurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private SurfaceHolder myholder;

    private EntryActivity parent;

    // Standard scale matrix
    private Matrix scaleMatrix;

    // Text painting
    private Paint textPaint;
    private int fontSize = 30;

    private Bitmap bitmap;
    private Bitmap splash;

    private Rect singleBorder;
    private Rect hostBorder;
    private Rect clientBorder;
    private Rect nameBorder;

    private int updateTime = 0;
    private final int totalSplashTime = 2000;

    public int getUpdateTime() {
        return updateTime;
    }

    public EntrySurfaceView(EntryActivity parent, int updateTime) {
        super(parent);
        this.parent = parent;
        this.updateTime = updateTime;

        myholder = this.getHolder();
        myholder.addCallback(this);

        scaleMatrix = new Matrix();
        scaleMatrix.postScale((float) Global.widthScale, (float) Global.heightScale);

        textPaint = new Paint();
        textPaint.setColor(Color.BLACK);
        textPaint.setAlpha(200);
        textPaint.setTextSize((int) (fontSize * Global.heightScale));
        textPaint.setAntiAlias(true);
        textPaint.setStyle(Paint.Style.FILL);

        singleBorder = new Rect(524, 164, 746, 209);
        hostBorder = new Rect(524, 255, 745, 299);
        clientBorder = new Rect(525, 339, 749, 391);
        nameBorder = new Rect(142, 372, 406, 411);
    }

    public boolean draw() {
        updateTime += Global.frameInterval;
        if (updateTime > totalSplashTime && Global.myName.length() == 0) {
            Global.myName = "Name";
            // No preference, ask for a name.
            parent.queryName();
        }
        Canvas canvas = null;
        try {
            canvas = myholder.lockCanvas();
            if (updateTime <= totalSplashTime) {
                canvas.drawBitmap(splash, scaleMatrix, null);
            } else {
                canvas.drawBitmap(bitmap, scaleMatrix, null);
                DrawUtils.drawTextCenterAlignBase(canvas, Global.myName, textPaint, 400, 145, 404);
            }
        } catch (Exception ex) {
        } finally {
            if (canvas != null)
                myholder.unlockCanvasAndPost(canvas);
        }
        return true;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // Nothing to do.
    }

    private void initializeBitmap() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        // Bitmaps
        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.lobby, options);
        splash = BitmapFactory.decodeResource(getResources(), R.drawable.splash, options);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        initializeBitmap();
        new Thread(parent).start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // Nothing to do.
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != MotionEvent.ACTION_DOWN)
            return false;
        if (updateTime <= totalSplashTime)
            return false;
        double x = event.getX() / Global.widthScale, y = event.getY() / Global.heightScale;
        if (singleBorder.contains((int) x, (int) y)) {
            parent.onSingleClick(this);
            return true;
        }
        if (clientBorder.contains((int) x, (int) y)) {
            parent.onJoinClick(this);
            return true;
        }
        if (hostBorder.contains((int) x, (int) y)) {
            parent.onHostClick(this);
            return true;
        }
        if (nameBorder.contains((int) x, (int) y)) {
            parent.queryName();
            return true;
        }
        return false;
    }
}
