package tw.nmlab.piquok.game;

import tw.nmlab.piquok.Global;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;

public class AnimatedSprite {
    private Bitmap[] animation;
    private int numFrames;
    private int currentFrame;
    private int spriteHeight;
    private int spriteWidth;
    private int currentTime;
    private int timeInterval;

    public AnimatedSprite() {
        currentFrame = 0;
        currentTime = 0;
    }

    public void reset() {
        currentFrame = 0;
        currentTime = 0;
    }

    public void Initialize(Bitmap[] bitmap, double fps) {
        this.animation = bitmap;
        this.spriteHeight = bitmap[0].getHeight();
        this.spriteWidth = bitmap[0].getWidth();
        this.numFrames = bitmap.length;
        timeInterval = (int) (1000.0 / fps);
    }

    public int getSpriteHeight() {
        return spriteHeight;
    }

    public int getSpriteWidth() {
        return spriteWidth;
    }

    public void update() {
        currentTime += Global.frameInterval;
        if (currentTime >= timeInterval) {
            currentTime -= timeInterval;
            currentFrame++;

            if (currentFrame == numFrames) {
                currentFrame = 0;
            }
        }
    }

    public void draw(Canvas canvas, int xCenterPos, int yCenterPos) {
        RectF rect = new RectF((float) Global.widthScale * (xCenterPos - spriteWidth / 2),
                (float) Global.heightScale * (yCenterPos - spriteHeight / 2),
                (float) Global.widthScale * (xCenterPos + spriteWidth / 2),
                (float) Global.heightScale * (yCenterPos + spriteHeight / 2));
        canvas.drawBitmap(animation[currentFrame], null, rect, null);
    }
}
