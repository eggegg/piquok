package tw.nmlab.piquok.game;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.R;
import tw.nmlab.piquok.game.Player.PlayerType;
import tw.nmlab.piquok.game.item.Effect;
import android.hardware.Sensor;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.MotionEvent;

public class ClientGameController extends GameController {
    private static final String TAG = "ClientGameController";

    public ClientGameController(GameActivity parent) {
        this.parent = parent;
    }

    @Override
    public void setItemSpawnRate(double itemSpawnRate) {
        // Nothing to do.
    }

    private int[] colors = null;

    public void setColors(int[] colors) {
        this.colors = colors;
    }

    @Override
    public int[] countColors() {
        return colors;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!parent.flagGamePlaying) {
            return false;
        }
        if (player[playerId].getType() != PlayerType.PLAYER) {
            return false;
        }
        // 0: forward, -1: turn right, 1: turn left
        double x = event.getX();
        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN:
        case MotionEvent.ACTION_MOVE:
            if (x <= Global.windowWidth / 2) {
                Global.clientConnection.send(Global.ACTION_CHANGE_DIRECTION,
                        new String[] { String.valueOf(playerId), String.valueOf(1) });
            } else {
                Global.clientConnection.send(Global.ACTION_CHANGE_DIRECTION,
                        new String[] { String.valueOf(playerId), String.valueOf(-1) });
            }
            break;
        case MotionEvent.ACTION_UP:
            Global.clientConnection.send(Global.ACTION_CHANGE_DIRECTION,
                    new String[] { String.valueOf(playerId), String.valueOf(0) });
            break;
        }
        return true;
    }

    public void setStartFly(int id) {
        if (player[id].getType() == PlayerType.NONE) {
            Log.e(TAG, "Got player NONE start flying.");
            return;
        }
        player[id].startFlying();
    }

    public void setPlayerInfo(int id, double xPos, double yPos, int mode, double faceAngle,
            int effect, int effectTime) {
        if (player[id].getType() == PlayerType.NONE) {
            Log.e(TAG, "Got player NONE info.");
            return;
        }
        player[id].setxPos(xPos);
        player[id].setyPos(yPos);
        player[id].setMode(mode);
        player[id].setFaceAngle(faceAngle);
        player[id].setEffect(Effect.getEffectById(effect), effectTime);
    }

    public void playBounceSound() {
        musicPlayer = MediaPlayer.create(parent, R.raw.bounce);
        musicPlayer.start();
    }

    public void playerGotItem(int playerId, int itemId) {
        musicPlayer = MediaPlayer.create(parent, R.raw.prize);
        musicPlayer.start();
        myView.eventPlayerGotItem(playerId, allItems[itemId]);
        currentItem = null;
    }

    public void setItemInfo(int itemid, int xPos, int yPos) {
        currentItem = allItems[itemid];
        itemXPos = xPos;
        itemYPos = yPos;
    }

    public boolean gameEnded = false;

    public void setUpdateNumber(int updateNumber) {
        this.updateNumber = updateNumber;
    }

    @Override
    public void update() {
        if (!gameStarted) {
            return;
        }
        if (gameEnded) {
            parent.flagGamePlaying = false;
            isGameCompleted = true;
            return;
        }
        if (updatePause()) {
            return;
        }
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (player[i].isAvailable())
                player[i].updateSprite();
        }
    }

    public void disconnected() {
        parent.disconnected();
    }

    @Override
    protected void onShake() {
        if (!parent.flagGamePlaying) {
            return;
        }
        if (player[playerId] == null || player[playerId].getType() != PlayerType.PLAYER) {
            return;
        }
        Global.clientConnection.send(Global.ACTION_CHANGE_MODE,
                new String[] { String.valueOf(playerId) });
    }

    public void playerDisconnected(int playerId) {
        parent.makeToast(String.format("%s had disconnected, and is controlled by AI now.",
                player[playerId].getName()));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
