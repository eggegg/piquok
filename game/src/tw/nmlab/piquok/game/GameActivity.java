package tw.nmlab.piquok.game;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.R;
import tw.nmlab.piquok.game.Player.PlayerType;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class GameActivity extends Activity implements Runnable {
    private static final String TAG = "GameActivity";
    public boolean flagGamePlaying;
    private GameSurfaceView mySufView;
    private GameController gc;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private boolean isHost;
    private MediaPlayer musicPlayer;
    private MediaPlayer musicPlayer2;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // For displaying toast.
        HandlerThread uiThread = new HandlerThread("UIHandler");
        uiThread.start();
        uiHandler = new UIHandler(uiThread.getLooper());

        musicPlayer = MediaPlayer.create(this, R.raw.gaming);
        musicPlayer.setLooping(true);
        musicPlayer.start();

        Bundle bundle = getIntent().getExtras();

        flagGamePlaying = true;

        isHost = bundle.getBoolean("isHost");

        PlayerType[] type = new PlayerType[Global.maxPlayer];
        String[] playerName = new String[Global.maxPlayer];
        int count = 0;
        for (int i = 0; i < Global.maxPlayer; i++) {
            type[i] = PlayerType.getTypeById(bundle.getInt(String.format("playerType%d", i)));
            if (type[i] == PlayerType.PLAYER)
                count++;
            playerName[i] = bundle.getString(String.format("playerName%d", i));
        }
        int graphicsQuality = bundle.getInt("graphicsQuality");

        if (isHost) {
            gc = new ServerGameController(this, count);
        } else {
            gc = new ClientGameController(this);
        }
        mySufView = new GameSurfaceView(this, graphicsQuality);

        gc.setPlayerType(type);
        gc.setPlayerNames(playerName);
        gc.setGameLength(bundle.getDouble("gameLength"));
        gc.setItemSpawnRate(bundle.getDouble("itemSpawnRate"));
        gc.setPlayerId(bundle.getInt("playerId"));
        gc.isHost = isHost;
        gc.isSinglePlayer = bundle.getBoolean("isSinglePlayer");

        gc.setView(mySufView);
        mySufView.setController(gc);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(gc, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        if (isHost) {
            if (Global.serverConnection != null) {
                Global.serverConnection.registerGameController((ServerGameController) gc);
            }
        } else {
            if (Global.clientConnection != null) {
                Global.clientConnection.registerGameController((ClientGameController) gc);
            } else {
                Log.e(TAG, "Client doesn't have a connection.");
            }
            Global.clientConnection.send(Global.RESPONSE_GAME_LOADED, null);
        }

        setContentView(mySufView);
    }

    @Override
    public void run() {
        musicPlayer2 = MediaPlayer.create(this, R.raw.start);
        musicPlayer2.start();
        Log.i(TAG, "GameThread started");
        long beginTime = System.currentTimeMillis();
        while (flagGamePlaying) {
            long c1 = System.currentTimeMillis();
            gc.update();
            mySufView.draw();
            long c2 = System.currentTimeMillis();
            try {
                Thread.sleep(Math.max(1, Global.frameInterval - (c2 - c1)));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        musicPlayer.stop();
        Log.i(TAG, String.format("Total Game Time: %d", System.currentTimeMillis() - beginTime));
        if (gc.isGameCompleted) {
            musicPlayer = MediaPlayer.create(this, R.raw.end);
            musicPlayer.start();
            mySufView.drawEndGame();
        } else if (!isHost) {
            makeToast("Disconnected.");
        }
        if (isHost) {
            if (Global.serverConnection != null) {
                Global.serverConnection.end();
                Global.serverConnection = null;
            }
        } else {
            if (Global.clientConnection != null) {
                Global.clientConnection.end();
                Global.clientConnection = null;
            } else {
                Log.e(TAG, "Client doesn't have a connection.");
            }
        }
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(gc);
        flagGamePlaying = false;
    }

    @Override
    public void onBackPressed() {
        flagGamePlaying = false;
    }

    public void disconnected() {
        flagGamePlaying = false;
    }

    private final class UIHandler extends Handler {

        public UIHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            Context context = getApplicationContext();
            Toast t = Toast.makeText(context, (String) msg.obj, Toast.LENGTH_SHORT);
            t.show();
        }
    }

    private UIHandler uiHandler;

    public void makeToast(String message) {
        Message msg = uiHandler.obtainMessage();
        msg.obj = message;
        uiHandler.sendMessage(msg);
    }
}