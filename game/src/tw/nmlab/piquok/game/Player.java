package tw.nmlab.piquok.game;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.Utils;
import tw.nmlab.piquok.game.item.Effect;
import tw.nmlab.piquok.game.item.Item;
import tw.nmlab.piquok.game.item.ItemExplode;
import tw.nmlab.piquok.game.item.ItemSmoke;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.util.Log;

public final class Player {
    private double xPos, yPos;

    public void setxPos(double xPos) {
        this.xPos = xPos;
    }

    public void setyPos(double yPos) {
        this.yPos = yPos;
    }

    private double faceAngle;

    public void setFaceAngle(double faceAngle) {
        this.faceAngle = faceAngle;
    }

    private int id;

    public double getxPos() {
        return xPos;
    }

    public double getyPos() {
        return yPos;
    }

    private AnimatedSprite spriteNormal;
    private AnimatedSprite spriteFast;
    private AnimatedSprite spriteFlyingNormal;
    private AnimatedSprite spriteFlyingFast;
    private GameController parent;
    private GameSurfaceView view;
    private Bitmap arrow;
    private Bitmap arrowFast;
    private Bitmap shadow;

    private double baseTurnRate[] = { 0.1, 0.18 };
    private double baseSpeed[] = { 5.0, 8.0 };
    private double baseDrawRadius[] = { 30, 15 };
    private double baseCollisionRadius[] = { 30, 15 };

    private double turnRate;

    private Effect effect;

    public Effect getEffect() { // For AI purpose.
        return effect;
    }

    private int nowEffectTime;

    public void setEffect(Effect effect, int effectTime) {
        this.effect = effect;
        this.nowEffectTime = effectTime;
    }

    public void setEffect(Effect effect) {
        setEffect(effect, 0);
    }

    private double speed;
    private double drawRadius;
    private double collisionRadius;

    private int mode;

    private int direction = 0;

    // 0: forward, -1: turn right, 1: turn left

    public void setDirection(int direction) {
        if (effect == Effect.CONFUSED) {
            this.direction = -direction;
        } else {
            this.direction = direction;
        }
    }

    public double getCollisionRadius() {
        return collisionRadius;
    }

    private Paint paint;
    private int myColor;

    public boolean isFlying = false;
    private static double flyHeight = 50;
    private int totalFlyTime = 1500;
    private int nowFlyTime;

    public enum PlayerType {
        NONE(0), PLAYER(1), AI(2);
        private int id;

        PlayerType(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public static PlayerType getTypeById(int id) {
            return PlayerType.class.getEnumConstants()[id];
        }
    };

    private PlayerType type;

    public PlayerType getType() {
        return type;
    }

    public void setType(PlayerType type) {
        this.type = type;
    }

    public boolean isAvailable() {
        return type != PlayerType.NONE;
    }

    public Player(int id, double xPos, double yPos, double faceAngle, int color,
            GameController parent, GameSurfaceView view) {
        this.id = id;
        this.xPos = xPos;
        this.yPos = yPos;
        this.faceAngle = faceAngle;
        this.parent = parent;
        this.view = view;
        this.myColor = color;

        paint = new Paint();
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL);
        this.mode = 0;
        resetMode();
        this.effect = null;
    }

    // :D
    public final static String[] computerName = { "DeepBlue", "Watson", "HAL9000", "Skynet" };
    private String playerName = null;

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getName() {
        if (playerName == null) {
            if (type == PlayerType.PLAYER) {
                Log.e("Player", "Player with no name.");
                playerName = String.format("Player %d", id + 1);
                return playerName;
            } else if (type == PlayerType.AI) {
                return computerName[id];
            } else {
                throw new IllegalArgumentException();
            }
        } else {
            if (type == PlayerType.PLAYER) {
                return playerName;
            } else if (type == PlayerType.AI) {
                return playerName + "(AI)";
            } else {
                throw new IllegalArgumentException();
            }
        }
    }

    public double getSpeed() {
        double ret = speed;
        if (effect == Effect.SLOW)
            ret *= 0.6;
        else if (effect == Effect.SPEED)
            ret *= 1.5;
        else if (effect == Effect.ICED)
            ret = 0.0;
        return ret;
    }

    public void setSprites(AnimatedSprite spriteNormal, AnimatedSprite spriteFast,
            AnimatedSprite spriteFlyingNormal, AnimatedSprite spriteFlyingFast, Bitmap arrow,
            Bitmap arrowFast, Bitmap shadow) {
        this.spriteNormal = spriteNormal;
        this.spriteFast = spriteFast;
        this.spriteFlyingNormal = spriteFlyingNormal;
        this.spriteFlyingFast = spriteFlyingFast;
        this.arrow = arrow;
        this.arrowFast = arrowFast;
        this.shadow = shadow;
    }

    public void updateSprite() {
        if (effect != null) {
            effect.update(id);
        }
        if (bumpEffect != null) {
            bumpEffect.update(id);
        }
        if (isFlying) {
            nowFlyTime += Global.frameInterval;
            if (nowFlyTime >= totalFlyTime) {
                isFlying = false;
            }
        }
        if (isFlying) {
            if (effect != null && effect.isBuff()) {
                effect = null;
            }
            if (getMode() == 0) {
                spriteFlyingNormal.update();
            } else if (getMode() == 1) {
                spriteFlyingFast.update();
            }
        } else {
            if (getMode() == 0) {
                spriteNormal.update();
            } else if (getMode() == 1) {
                spriteFast.update();
            }
        }

    }

    public void update() {
        faceAngle -= direction * turnRate;
        faceAngle = Utils.Arg(faceAngle);
        xPos += getSpeed() * Math.cos(faceAngle);
        yPos += getSpeed() * Math.sin(faceAngle);
        xPos = Utils.minmax(xPos, spriteNormal.getSpriteWidth() / 2, Global.baseWindowWidth
                - spriteNormal.getSpriteWidth() / 2);
        yPos = Utils.minmax(yPos, spriteNormal.getSpriteHeight() / 2, Global.baseWindowHeight
                - spriteNormal.getSpriteHeight() / 2);

        if (effect != null) {
            effect.update(id);
            nowEffectTime += Global.frameInterval;
            if (nowEffectTime >= effect.getMaxDuration()) {
                // Effect times up.
                effect = null;
            }
        }
        if (bumpEffect != null) {
            bumpEffect.update(id);
            nowBumpEffectTime += Global.frameInterval;
            if (nowBumpEffectTime >= bumpEffect.getMaxDuration()) {
                // Effect times up.
                bumpEffect = null;
            }
        }
        if (isFlying) {
            nowFlyTime += Global.frameInterval;
            if (nowFlyTime >= totalFlyTime) {
                isFlying = false;
                setEffect(Effect.SLOW);
            }
        }
        if (isFlying) {
            if (effect != null && effect.isBuff()) {
                effect = null;
            }
            if (getMode() == 0) {
                spriteFlyingNormal.update();
            } else if (getMode() == 1) {
                spriteFlyingFast.update();
            }
        } else {
            if (getMode() == 0) {
                spriteNormal.update();
            } else if (getMode() == 1) {
                spriteFast.update();
            }
        }
    }

    public void draw(Canvas canvas) {

        Matrix matrix = new Matrix();
        if (getMode() == 0) {
            matrix.setTranslate((float) xPos - arrow.getWidth() / 2,
                    (float) yPos - arrow.getHeight() / 2);
            matrix.postRotate((float) (faceAngle / Math.PI * 180.0), (float) xPos, (float) yPos);
            matrix.postScale((float) Global.widthScale, (float) Global.heightScale);
            canvas.drawBitmap(arrow, matrix, null);
        } else {
            matrix.setTranslate((float) xPos - arrowFast.getWidth() / 2,
                    (float) yPos - arrowFast.getHeight() / 2);
            matrix.postRotate((float) (faceAngle / Math.PI * 180.0), (float) xPos, (float) yPos);
            matrix.postScale((float) Global.widthScale, (float) Global.heightScale);
            canvas.drawBitmap(arrowFast, matrix, null);
        }

        matrix.reset();
        matrix.setTranslate((float) xPos - shadow.getWidth() / 2, (float) yPos - shadow.getHeight()
                / 2);
        matrix.postScale((float) Global.widthScale, (float) Global.heightScale);
        canvas.drawBitmap(shadow, matrix, null);

        double realY = yPos - 25;
        Effect effect = this.effect;
        if (effect != null && bumpEffect != null) {
            effect = bumpEffect;
        }
        if (isFlying) {
            double y = (1 - Math.pow((2.0 * nowFlyTime - totalFlyTime) / totalFlyTime, 2.0))
                    * flyHeight;
            realY -= y;
            if (getMode() == 0) {
                spriteFlyingNormal.draw(canvas, (int) xPos, (int) realY);
            } else if (getMode() == 1) {
                spriteFlyingFast.draw(canvas, (int) xPos, (int) realY);
            }
        } else {
            if (effect != null && effect.isReplaced()) {
                effect.draw(canvas, id, (int) xPos, (int) realY);
            } else if (getMode() == 0) {
                spriteNormal.draw(canvas, (int) xPos, (int) realY);
            } else if (getMode() == 1) {
                spriteFast.draw(canvas, (int) xPos, (int) realY);
            }
        }
        if (effect != null && !effect.isReplaced()) {
            effect.draw(canvas, id, (int) xPos, (int) realY);
        }
    }

    private Effect bumpEffect = null;
    private int nowBumpEffectTime;

    public void startFlying() {
        if (effect != Effect.SHIELD) {
            isFlying = true;
            nowFlyTime = 0;
            spriteFlyingNormal.reset();
            spriteFlyingFast.reset();

            // Broadcast this message.
            if (Global.serverConnection != null) {
                Global.serverConnection.broadcast(Global.RESPONSE_PLAYER_START_FLY,
                        new String[] { String.valueOf(id) });
            }
        } else {
            bumpEffect = Effect.SHIELD_BUMP;
            nowBumpEffectTime = 0;
        }
    }

    public void updateBumpDirection(Player p2) {
        double x2 = p2.getxPos(), y2 = p2.getyPos();
        double deg = Math.atan2(y2 - yPos, x2 - xPos);
        faceAngle = Utils.Arg(2 * deg - faceAngle - Math.PI);
    }

    public boolean isCollide(Player p2) {
        return Math.hypot(getxPos() - p2.getxPos(), getyPos() - p2.getyPos()) <= getCollisionRadius()
                + p2.getCollisionRadius();
    }

    public boolean isCollide(Item item, int x2, int y2) {
        return Math.hypot(getxPos() - x2, getyPos() - y2) <= getCollisionRadius()
                + item.getCollisionRadius();
    }

    public boolean canDraw() {
        return !isFlying;
    }

    public void drawBgColor(Canvas canvas, Canvas RealCanvas, Bitmap myBitmap) {
        if (canDraw()) {
            RectF rect = new RectF((float) (xPos - drawRadius), (float) (yPos - drawRadius),
                    (float) (xPos + drawRadius), (float) (yPos + drawRadius));
            canvas.drawOval(rect, paint);

            Path path = new Path();
            if (RealCanvas != null) {
                RealCanvas.save();
                path.addOval(rect, Direction.CW);
                RealCanvas.clipPath(path);
                RealCanvas.drawBitmap(myBitmap, new Matrix(), null);
                RealCanvas.restore();
            }

            if (effect == Effect.EXPLODING) {
                double r = ItemExplode.getExplodeRadius(nowEffectTime);
                rect = new RectF((float) (xPos - r), (float) (yPos - r), (float) (xPos + r),
                        (float) (yPos + r));
                canvas.drawOval(rect, paint);

                if (RealCanvas != null) {
                    RealCanvas.save();
                    path.reset();
                    path.addOval(rect, Direction.CW);
                    RealCanvas.clipPath(path);
                    RealCanvas.drawBitmap(myBitmap, new Matrix(), null);
                    RealCanvas.restore();
                }
            }
        }
    }

    public void resetMode() {
        turnRate = baseTurnRate[mode];
        speed = baseSpeed[mode];
        drawRadius = baseDrawRadius[mode];
        collisionRadius = baseCollisionRadius[mode];
    }

    // Broadcast all information of this player
    public void broadcast() {
        if (Global.serverConnection == null) {
            return;
        }
        if (type == PlayerType.NONE) {
            return;
        }
        Global.serverConnection.broadcast(
                Global.RESPONSE_PLAYER_INFO,
                new String[] { String.valueOf(id), String.valueOf(xPos), String.valueOf(yPos),
                        String.valueOf(mode), String.valueOf(faceAngle),
                        String.valueOf(effect == null ? -1 : effect.getId()),
                        String.valueOf(nowEffectTime) });
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int newMode) {
        mode = newMode;
        resetMode();
    }

    // Below is the AI part.
    private int AIFetchRange = 120;
    private int AIIgnoreRange = 30;
    private int AIFetchDensity = 10;
    private int AIHitDetectRange = 170;
    private double AIHitBuffWeight = 1500.0;
    private double AIHitAvoidWeight = 1000.0;
    private double AIHitWithShieldWeight = 4000.0;
    private double AIHitAvoidWithBuffWeight = 2000.0;
    private int AIGetItemRange = 400;
    private double AIGetItemWeight = 4000.0;
    private int AIEdgeAvoidRange = 80;
    private double AIEdgeAvoidWeight = 3500.0;
    private int AIUpdateInterval = 250;
    private int AIUpdateTimes = 0;
    private int[] AIEdgeAvoidDirection = { 1, 0, 0, 1 };

    public void updateAI() {
        AIUpdateTimes += Global.frameInterval;
        if (AIUpdateTimes < AIUpdateInterval)
            return;
        AIUpdateTimes -= AIUpdateInterval;
        Bitmap basebp = view.getBasebp();
        // Simple AI.
        int l = Math.max((int) (xPos - AIFetchRange), 0);
        int r = Math.min((int) (xPos + AIFetchRange), basebp.getWidth() - 1);
        int u = Math.max((int) (yPos - AIFetchRange), 0);
        int d = Math.min((int) (yPos + AIFetchRange), basebp.getHeight() - 1);
        int w = r - l + 1, h = d - u + 1;

        double[] count = new double[2];
        double[] fastcount = new double[2];

        // Colors
        for (int j = 0; j < h; j += AIFetchDensity) {
            for (int i = 0; i < w; i += AIFetchDensity) {
                int x = i + l, y = j + u;
                if ((x - xPos) * (x - xPos) + (y - yPos) * (y - yPos) < AIIgnoreRange
                        * AIIgnoreRange)
                    continue;
                int p = basebp.getPixel(x, y);
                if (p != myColor) {
                    double ang = Math.atan2(y - yPos, x - xPos);
                    int f;
                    double th = Utils.Arg(ang - faceAngle);
                    if (th < Math.PI) {
                        // On right hand side.
                        f = 0;
                    } else {
                        th = Math.PI * 2 - th;
                        f = 1;
                    }
                    if (p == Color.WHITE)
                        count[f] += 2 * (Math.PI - th + 1);
                    else
                        count[f] += 4 * (Math.PI - th + 1);
                }
            }
        }

        // Avoid hit.
        Player[] players = parent.getPlayers();
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (!players[i].isAvailable())
                continue;
            if (i == id)
                continue;
            double x = players[i].getxPos(), y = players[i].getyPos();
            if ((x - xPos) * (x - xPos) + (y - yPos) * (y - yPos) > AIHitDetectRange
                    * AIHitDetectRange)
                continue;
            double ang = Math.atan2(y - yPos, x - xPos);
            double th = Utils.Arg(ang - faceAngle);
            if (th < Math.PI) {
                // On right hand side.
                if (players[i].getEffect() != null && players[i].getEffect().isPositive()) {
                    fastcount[0] += AIHitBuffWeight;
                } else {
                    if (effect != Effect.SHIELD) {
                        if (effect != null && effect.isPositive()) {
                            fastcount[1] += AIHitAvoidWithBuffWeight;
                        } else {
                            count[1] += AIHitAvoidWeight;
                        }
                    } else {
                        fastcount[0] += AIHitWithShieldWeight;
                    }
                }
            } else {
                if (players[i].getEffect() != null && players[i].getEffect().isPositive()) {
                    fastcount[1] += AIHitBuffWeight;
                } else {
                    if (effect != Effect.SHIELD) {
                        if (effect != null && effect.isPositive()) {
                            fastcount[0] += AIHitAvoidWithBuffWeight;
                        } else {
                            count[0] += AIHitAvoidWeight;
                        }
                    } else {
                        fastcount[1] += AIHitWithShieldWeight;
                    }
                }
            }

        }

        // Get item.
        Item item = parent.getCurrentItem();
        if (item != null) {
            // There's an item on board.
            double x = parent.getItemXPos(), y = parent.getItemYPos();
            int nowViewRange = AIGetItemRange;
            if (effect == Effect.SMOKED && nowViewRange >= ItemSmoke.viewRadius) {
                nowViewRange = ItemSmoke.viewRadius;
            }
            if ((x - xPos) * (x - xPos) + (y - yPos) * (y - yPos) <= nowViewRange * nowViewRange) {
                double ang = Math.atan2(y - yPos, x - xPos);
                double th = Utils.Arg(ang - faceAngle);
                if (th < Math.PI) {
                    // On right hand side.
                    fastcount[0] += AIGetItemWeight;
                } else {
                    fastcount[1] += AIGetItemWeight;
                }
            }
        }

        // Avoid edge
        // Easy way to make it doesn't stuck at edge or corner, turn to id % 2
        // for some non-fixed pattern.
        if (yPos <= AIEdgeAvoidRange) {
            if (faceAngle >= Math.PI * 1.0 && faceAngle < Math.PI * 2.0)
                count[AIEdgeAvoidDirection[id]] += AIEdgeAvoidWeight;
        } else if (yPos >= Global.baseWindowHeight - AIEdgeAvoidRange) {
            if (faceAngle >= 0.0 && faceAngle < Math.PI * 1.0)
                count[AIEdgeAvoidDirection[id]] += AIEdgeAvoidWeight;
        } else if (xPos <= AIEdgeAvoidRange) {
            if (faceAngle >= Math.PI * 0.5 && faceAngle < Math.PI * 1.5)
                count[AIEdgeAvoidDirection[id]] += AIEdgeAvoidWeight;
        } else if (xPos >= Global.baseWindowWidth - AIEdgeAvoidRange) {
            if (faceAngle >= Math.PI * 1.5 || faceAngle < Math.PI * 0.5)
                count[AIEdgeAvoidDirection[id]] += AIEdgeAvoidWeight;
        }

        if (count[0] + fastcount[0] < count[1] + fastcount[1]) {
            direction = 1;
            if (fastcount[1] > count[1])
                setMode(1);
            else
                setMode(0);
        } else {
            direction = -1;
            if (fastcount[0] > count[0])
                setMode(1);
            else
                setMode(0);
        }
        if (effect == Effect.CONFUSED && Math.random() <= 0.2) {
            direction *= -1;
        }
    }
}
