package tw.nmlab.piquok.game;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.game.Player.PlayerType;
import tw.nmlab.piquok.game.item.Item;
import tw.nmlab.piquok.game.item.ItemConfusion;
import tw.nmlab.piquok.game.item.ItemExplode;
import tw.nmlab.piquok.game.item.ItemIce;
import tw.nmlab.piquok.game.item.ItemShield;
import tw.nmlab.piquok.game.item.ItemSmoke;
import tw.nmlab.piquok.game.item.ItemSpeed;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.media.MediaPlayer;
import android.view.MotionEvent;

public abstract class GameController implements SensorEventListener {

    protected GameActivity parent;
    protected GameSurfaceView myView;

    public boolean isHost;
    public boolean isSinglePlayer;
    public boolean isGameCompleted = false;

    public boolean gameStarted = false;
    protected MediaPlayer musicPlayer;

    // Player data
    protected Player player[] = new Player[4];
    protected int playerId = 0;

    protected static int playerColor[] = { Color.rgb(88, 142, 195), Color.rgb(136, 174, 87),
            Color.rgb(225, 218, 69), Color.rgb(200, 97, 80) };

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public Player[] getPlayers() { // For AI purpose
        return player;
    }

    public Player getPlayerById(int id) { // For AI purpose
        return player[id];
    }

    public static int getPlayerColorById(int playerId) {
        return playerColor[playerId];
    }

    protected PlayerType[] types = null;
    protected String[] playerNames = null;

    public void setPlayerType(PlayerType[] types) {
        this.types = types;
    }

    public void setPlayerNames(String[] playerNames) {
        this.playerNames = playerNames;
    }

    // Game timer
    protected int updateNumber = 0;
    protected double gameLength = 30.0;

    public void setGameLength(double gameLength) {
        this.gameLength = gameLength;
    }

    public double getElapsedTime() {
        return (double) updateNumber * Global.frameInterval / 1000.0;
    }

    public double getLeftTime() {
        return gameLength - getElapsedTime();
    }

    public int getUpdateNumber() {
        return updateNumber;
    }

    protected Item currentItem = null;
    protected int itemXPos, itemYPos;

    public void setView(GameSurfaceView myView) {
        this.myView = myView;
    }

    public Item getCurrentItem() { // For AI purpose;
        return currentItem;
    }

    public int getItemXPos() {
        return itemXPos;
    }

    public int getItemYPos() {
        return itemYPos;
    }

    protected Item[] allItems;

    protected void initializeItems() {
        // Should be called after myView is set.
        allItems = new Item[] { new ItemSpeed(myView), new ItemIce(myView),
                new ItemConfusion(myView), new ItemExplode(myView), new ItemSmoke(myView),
                new ItemShield(myView) };
        for (int i = 0; i < allItems.length; i++)
            allItems[i].setId(i);
    }

    protected int startPosition[][] = { { -1, -1 }, { 1, -1 }, { -1, 1 }, { 1, 1 } };
    protected double startAngle[] = { Math.PI * 5. / 4., Math.PI * 7. / 4., Math.PI * 3. / 4.,
            Math.PI * 1. / 4. };

    protected void initializePlayer(PlayerType[] playertypes) {
        for (int i = 0; i < Global.maxPlayer; i++) {
            player[i] = new Player(i, Global.baseWindowWidth / 2 + startPosition[i][0] * 60,
                    Global.baseWindowHeight / 2 + startPosition[i][1] * 60, startAngle[i],
                    playerColor[i], this, myView);
            player[i].setType(playertypes[i]);
            if (playertypes[i] == PlayerType.PLAYER)
                player[i].setPlayerName(playerNames[i]);
        }
    }

    public void initialize() { // Called after SurfaceView is ready.
        initializePlayer(types);
        initializeItems();
    }

    public abstract void update();

    protected int totalPauseTime;

    public void pause(int totalPauseTime) {
        this.totalPauseTime = totalPauseTime;
    }

    protected boolean updatePause() {
        // Return true if it's currently pausing,
        // and update the remaining pause time.
        if (totalPauseTime <= 0)
            return false;
        totalPauseTime -= Global.frameInterval;
        return true;
    }

    public boolean isPausing() {
        return totalPauseTime > 0;
    }

    // Sensors
    private long lastSensorUpdate = 0;
    private long lastSpeedChange = 0;
    private double lastSensorX = 0, lastSensorY = 0, lastSensorZ = 0;
    private final double SHAKE_THRESHOLD = 500;

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            long curTime = System.currentTimeMillis();
            // only allow one update every 100ms.
            if ((curTime - lastSensorUpdate) > 100) {
                long diffTime = (curTime - lastSensorUpdate);
                lastSensorUpdate = curTime;

                double x = event.values[0];
                double y = event.values[1];
                double z = event.values[2];

                double speed = Math.abs(x + y + z - lastSensorX - lastSensorY - lastSensorZ)
                        / diffTime * 10000;
                if (speed > SHAKE_THRESHOLD && (curTime - lastSpeedChange) > 300) {
                    lastSpeedChange = curTime;
                    onShake();
                }
                lastSensorX = x;
                lastSensorY = y;
                lastSensorZ = z;
            }
        }
    }

    protected abstract void onShake();

    public abstract void setItemSpawnRate(double itemSpawnRate);

    public abstract int[] countColors();

    public abstract boolean onTouchEvent(MotionEvent event);
}
