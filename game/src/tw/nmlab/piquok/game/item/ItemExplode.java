package tw.nmlab.piquok.game.item;

import tw.nmlab.piquok.R;
import tw.nmlab.piquok.game.AnimatedSprite;
import tw.nmlab.piquok.game.GameController;
import tw.nmlab.piquok.game.GameSurfaceView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;

public class ItemExplode extends Item {

    public ItemExplode(View view) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        sprite = new AnimatedSprite();
        sprite.Initialize(new Bitmap[] { BitmapFactory.decodeResource(view.getResources(),
                R.drawable.item_explosion, options) }, 0.01);
    }

    @Override
    public void affect(GameController gc, GameSurfaceView view, int playerID) {
        gc.getPlayerById(playerID).setEffect(Effect.EXPLODING);
    }

    public static int totalExplodeTime = 500;
    private static int explodeRadius = 140;

    public static double getExplodeRadius(int time) {
        double x = (double) time / totalExplodeTime;
        return explodeRadius * Math.pow(x, 0.2);
    }

    @Override
    public String getName() {
        // TODO better name :D
        return "Explosion";
    }

}
