package tw.nmlab.piquok.game.item;

import java.util.ArrayList;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.game.AnimatedSprite;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.View;

public class Effect {
    // Enumerate-like class for effects

    private static ArrayList<Effect> allEffect;

    public static final Effect SPEED;
    public static final Effect SLOW;
    public static final Effect ICED;
    public static final Effect CONFUSED;
    public static final Effect EXPLODING;
    public static final Effect SMOKED;
    public static final Effect SHIELD;
    public static final Effect SHIELD_BUMP;
    static {
        allEffect = new ArrayList<Effect>();
        SPEED = new Effect(10000, "effect_speed", 24, 0, 0, 10.0, true, false, true);
        SLOW = new Effect(3000, "effect_slowed", 14, 0, -30, 10.0, false, false, true);
        ICED = new Effect(5000, "effect_iced", 1, 0, 0, 0.01, false, false, true);
        CONFUSED = new Effect(10000, "effect_confused", 16, 0, -10, 10.0, false, false, true);

        EXPLODING = new Effect(ItemExplode.totalExplodeTime, null, 0, 0, 0, 0, false, false, false);
        SMOKED = new Effect(10000, "effect_smoked", 12, 0, 0, 10.0, false, false, false);
        
        SHIELD = new Effect(7000, "effect_shield", 30, 0, 0, 20.0, false, false, false);
        SHIELD_BUMP = new Effect(550, "effect_shieldbump", 11, 0, 0, 20.0, false, false, false);
    }
    private int id;

    public int getId() {
        return id;
    }

    // in milliseconds.
    private int duration;
    private String spriteFile;
    private int spriteCount;
    private AnimatedSprite[] sprite;
    private int dx, dy;
    private double fps;
    // For AI purpose.
    private boolean isPositive;
    private boolean isReplaced;
    private boolean isBuff;

    public boolean isPositive() {
        return isPositive;
    }

    public boolean isReplaced() {
        return isReplaced;
    }

    public boolean isBuff() {
        return isBuff;
    }

    public int getMaxDuration() {
        return duration;
    }

    public void draw(Canvas canvas, int playerId, int xPos, int yPos) {
        if (spriteFile != null) {
            sprite[playerId].draw(canvas, xPos + dx, yPos + dy);
        }
    }

    public static Effect getEffectById(int id) {
        if (id == -1)
            return null;
        return allEffect.get(id);
    }

    private Effect(int duration, String spriteFile, int spriteCount, int dx, int dy, double fps,
            boolean isPositive, boolean isReplaced, boolean isBuff) {
        this.duration = duration;
        this.spriteFile = spriteFile;
        this.spriteCount = spriteCount;
        this.dx = dx;
        this.dy = dy;
        this.fps = fps;
        this.isPositive = isPositive;
        this.isReplaced = isReplaced;
        this.isBuff = isBuff;
        this.id = allEffect.size();
        allEffect.add(this);
    }

    private void initSprite(View msv) {
        if (spriteFile != null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            sprite = new AnimatedSprite[Global.maxPlayer];
            for (int j = 0; j < Global.maxPlayer; j++) {
                Bitmap bp[] = new Bitmap[spriteCount];
                for (int i = 0; i < spriteCount; i++) {
                    // TODO dynamic package name
                    if (isReplaced()) {
                        bp[i] = BitmapFactory.decodeResource(
                                msv.getResources(),
                                msv.getResources().getIdentifier(
                                        String.format("%s%d_%d", spriteFile, j + 1, i + 1),
                                        "drawable", "tw.nmlab.piquok"), options);
                    } else {
                        // Same for everyone
                        bp[i] = BitmapFactory.decodeResource(
                                msv.getResources(),
                                msv.getResources().getIdentifier(
                                        String.format("%s%d", spriteFile, i + 1), "drawable",
                                        "tw.nmlab.piquok"), options);
                    }
                }
                sprite[j] = new AnimatedSprite();
                sprite[j].Initialize(bp, fps);
            }
        }
    }

    public static void initialize(View msv) {
        for (int i = 0; i < allEffect.size(); i++) {
            allEffect.get(i).initSprite(msv);
        }
    }

    public void update(int playerId) {
        if (spriteFile != null) {
            sprite[playerId].update();
        }
    }
}
