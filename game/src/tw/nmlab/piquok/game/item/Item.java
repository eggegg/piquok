package tw.nmlab.piquok.game.item;

import tw.nmlab.piquok.game.AnimatedSprite;
import tw.nmlab.piquok.game.GameController;
import tw.nmlab.piquok.game.GameSurfaceView;
import android.graphics.Canvas;

public abstract class Item {

    protected AnimatedSprite sprite;

    // For sending & receiving
    protected int id;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getCollisionRadius() {
        // Default size
        return 30;
    }

    public void reset() {
        // Default action
        sprite.reset();
    }

    public void update() {
        // Default action
        sprite.update();
    }

    public void draw(Canvas canvas, int xPos, int yPos) {
        // Default action
        sprite.draw(canvas, xPos, yPos);
    }

    public abstract void affect(GameController gc, GameSurfaceView view, int playerID);

    public abstract String getName();
}
