package tw.nmlab.piquok.game.item;

import tw.nmlab.piquok.R;
import tw.nmlab.piquok.game.AnimatedSprite;
import tw.nmlab.piquok.game.GameController;
import tw.nmlab.piquok.game.GameSurfaceView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;

public class ItemShield extends Item {

    public ItemShield(View view) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        sprite = new AnimatedSprite();
        sprite.Initialize(new Bitmap[] { BitmapFactory.decodeResource(view.getResources(),
                R.drawable.item_shield, options) }, 0.01);
    }

    @Override
    public void affect(GameController gc, GameSurfaceView view, int playerID) {
        gc.getPlayerById(playerID).setEffect(Effect.SHIELD);
    }

    @Override
    public String getName() {
        // TODO better name :D
        return "Shield";
    }

}
