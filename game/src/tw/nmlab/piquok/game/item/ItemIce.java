package tw.nmlab.piquok.game.item;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.R;
import tw.nmlab.piquok.game.AnimatedSprite;
import tw.nmlab.piquok.game.GameController;
import tw.nmlab.piquok.game.GameSurfaceView;
import tw.nmlab.piquok.game.Player;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;

public class ItemIce extends Item {

    public ItemIce(View view) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        sprite = new AnimatedSprite();
        sprite.Initialize(new Bitmap[] { BitmapFactory.decodeResource(view.getResources(),
                R.drawable.item_ice, options) }, 0.01);
    }

    @Override
    public void affect(GameController gc, GameSurfaceView view, int playerID) {
        for (int i = 0; i < Global.maxPlayer; i++) {
            Player player = gc.getPlayerById(i);
            if (i != playerID && player.isAvailable()) {
                player.setEffect(Effect.ICED);
            }
        }
    }

    @Override
    public String getName() {
        // TODO better name :D
        return "Ice Storm";
    }

}
