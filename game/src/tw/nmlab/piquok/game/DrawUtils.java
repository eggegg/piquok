package tw.nmlab.piquok.game;

import tw.nmlab.piquok.Global;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class DrawUtils {
    public static void drawTextLeftAlign(Canvas canvas, String text, Paint paint, double xPos,
            double yPos) {
        canvas.drawText(text, (float) (xPos * Global.widthScale),
                (float) (yPos * Global.heightScale) - paint.getFontMetrics().ascent, paint);
    }

    public static void drawTextLeftAlignBase(Canvas canvas, String text, Paint paint, double xPos,
            double yBasePos) {
        canvas.drawText(text, (float) (xPos * Global.widthScale),
                (float) (yBasePos * Global.heightScale), paint);
    }

    public static void drawTextsLeftAlignBase(Canvas canvas, String[] text, Paint[][] paint,
            double xPos, double yBasePos, double skip) {
        // Assuming all paint of the same first index is of same size.
        int n = text.length;
        if (n != paint.length)
            throw new IllegalArgumentException();
        xPos *= Global.widthScale;
        skip *= Global.widthScale;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < paint[i].length; j++) {
                canvas.drawText(text[i], (float) xPos, (float) (yBasePos * Global.heightScale),
                        paint[i][j]);
            }
            xPos += paint[i][0].measureText(text[i]) + skip;
        }
    }

    public static void drawTextsCenterAlignBase(Canvas canvas, String[] text, Paint[][] paint,
            double yBasePos, double skip) {
        // Assuming all paint of the same first index is of same size.
        int n = text.length;
        if (n != paint.length)
            throw new IllegalArgumentException();
        float[] width = new float[n];
        skip *= Global.widthScale;
        double totalLength = (n - 1) * skip;
        for (int i = 0; i < n; i++) {
            width[i] = paint[i][0].measureText(text[i]);
            totalLength += width[i];
        }
        double xPos = 0.5 * (Global.windowWidth - totalLength);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < paint[i].length; j++) {
                canvas.drawText(text[i], (float) xPos, (float) (yBasePos * Global.heightScale),
                        paint[i][j]);
            }
            xPos += width[i] + skip;
        }
    }

    public static void drawTextRightAlign(Canvas canvas, String text, Paint paint, double xPos,
            double yPos) {
        double width = paint.measureText(text);
        canvas.drawText(text, (float) (xPos * Global.widthScale - width),
                (float) (yPos * Global.heightScale) - paint.getFontMetrics().ascent, paint);
    }

    public static void drawTextRightAlignBase(Canvas canvas, String text, Paint paint, double xPos,
            double yPos) {
        double width = paint.measureText(text);
        canvas.drawText(text, (float) (xPos * Global.widthScale - width),
                (float) (yPos * Global.heightScale), paint);
    }

    public static void drawTextCenterAlign(Canvas canvas, String text, Paint paint, double yPos) {
        drawTextCenterAlign(canvas, text, paint, yPos, 0, Global.baseWindowWidth);
    }

    public static void drawTextCenterAlign(Canvas canvas, String text, Paint paint, double yPos,
            double xLeft, double xRight) {
        double width = paint.measureText(text);
        canvas.drawText(text, (float) (0.5 * ((xRight - xLeft) * Global.widthScale - width) + xLeft
                * Global.widthScale), (float) (yPos * Global.heightScale)
                - paint.getFontMetrics().ascent, paint);
    }

    public static void drawTextCenterAlignBase(Canvas canvas, String text, Paint paint, double yPos) {
        drawTextCenterAlignBase(canvas, text, paint, yPos, 0, Global.baseWindowWidth);
    }

    public static void drawTextCenterAlignBase(Canvas canvas, String text, Paint paint,
            double yPos, double xLeft, double xRight) {
        double width = paint.measureText(text);
        canvas.drawText(text, (float) (0.5 * ((xRight - xLeft) * Global.widthScale - width) + xLeft
                * Global.widthScale), (float) (yPos * Global.heightScale), paint);
    }

    public static void drawTextCenterScreen(Canvas canvas, String text, Paint paint) {
        Rect rect = new Rect();
        paint.getTextBounds(text, 0, text.length(), rect);
        canvas.drawText(text, (float) (0.5 * (Global.windowWidth - (rect.right - rect.left)))
                - rect.left, (float) (0.5 * (Global.windowHeight - (rect.bottom - rect.top)))
                - rect.top, paint);
    }

    public static Rect getTextBoundBase(Paint paint, String text, double xPos, double yPos) {
        Rect rect = new Rect();
        rect.left = (int) xPos;
        rect.right = (int) (xPos + paint.measureText(text) / Global.widthScale);
        rect.top = (int) (yPos + paint.getFontMetrics().ascent / Global.widthScale);
        rect.bottom = (int) (yPos + paint.getFontMetrics().descent / Global.widthScale);
        return rect;
    }

    public static Rect getTextBoundBaseCenter(Paint paint, String text, double xPos, double yPos) {
        Rect rect = new Rect();
        rect.left = (int) (xPos - paint.measureText(text) / 2.0 / Global.widthScale);
        rect.right = (int) (xPos + paint.measureText(text) / 2.0 / Global.widthScale);
        rect.top = (int) (yPos + paint.getFontMetrics().ascent / Global.widthScale);
        rect.bottom = (int) (yPos + paint.getFontMetrics().descent / Global.widthScale);
        return rect;
    }

    public static void drawRect(Canvas canvas, Rect r, Paint paint) {
        drawRect(canvas, r, paint, false);
    }

    public static void drawRect(Canvas canvas, Rect r, Paint paint, boolean isScaled) {
        Rect rect = new Rect(r);
        if (!isScaled) {
            rect.left *= Global.widthScale;
            rect.right *= Global.widthScale;
            rect.top *= Global.heightScale;
            rect.bottom *= Global.heightScale;
        }
        canvas.drawRect(rect, paint);
    }
}
