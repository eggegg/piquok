package tw.nmlab.piquok.game;

import java.util.Arrays;
import java.util.Comparator;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.R;
import tw.nmlab.piquok.game.item.Effect;
import tw.nmlab.piquok.game.item.Item;
import tw.nmlab.piquok.game.item.ItemSmoke;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Typeface;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = "MySurfaceView";

    private SurfaceHolder myholder;

    private GameActivity parent;
    private GameController gc;

    public void setController(GameController gc) {
        this.gc = gc;
    }

    private Bitmap basebp;
    private Bitmap baseRealbp;
    private Bitmap baseColorbp[];
    private Bitmap smokebp;

    public Bitmap getBasebp() { // For AI purpose
        return basebp;
    }

    // Canvas for basebp
    private Canvas baseCanvas;
    private Canvas baseRealCanvas;

    // Standard scale matrix
    private Matrix scaleMatrix;

    // Text painting
    private Paint textPaint;
    private int fontSize = 30;
    private int bigFontSize = 60;
    private int graphicsQuality;

    public GameSurfaceView(GameActivity parent, int graphicsQuality) {
        super(parent);
        this.parent = parent;
        this.graphicsQuality = graphicsQuality;

        myholder = this.getHolder();
        myholder.addCallback(this);

        scaleMatrix = new Matrix();
        scaleMatrix.postScale((float) Global.widthScale, (float) Global.heightScale);

        textPaint = new Paint();
        textPaint.setColor(Color.BLACK);
        textPaint.setAlpha(200);
        textPaint.setTextSize((int) (fontSize * Global.heightScale));
        textPaint.setAntiAlias(true);
        textPaint.setStyle(Paint.Style.FILL);
    }

    public static final int itemMessageTime = 1500;
    private int gotItemPlayerId;
    private String gotItemName = null;

    public void eventPlayerGotItem(int playerId, Item item) {
        gc.pause(itemMessageTime);
        gotItemPlayerId = playerId;
        gotItemName = item.getName();
    }

    private String[] countdownMessage = { "3", "2", "1", "Go!" };
    private int countdownNumber = 0;

    public void eventCountdownTimer() {
        gc.pause(1000);
    }

    public void draw() {
        if (!gc.gameStarted) {
            Canvas canvas = null;
            try {
                canvas = myholder.lockCanvas();
                canvas.drawColor(Color.BLACK);
                Paint paint = getTextPaint(1.0);
                paint.setARGB(255, 224, 224, 224);
                DrawUtils.drawTextCenterScreen(canvas, "Loading...", paint);
            } catch (Exception ex) {
            } finally {
                if (canvas != null)
                    myholder.unlockCanvasAndPost(canvas);
            }
            return;
        }
        // Sort players for drawing.
        Integer allp[] = new Integer[Global.maxPlayer];
        int playerCount = 0;
        final double playerYPos[] = new double[4];
        for (int i = 0; i < Global.maxPlayer; i++) {
            Player player = gc.getPlayerById(i);
            if (player.isAvailable()) {
                allp[playerCount++] = i;
                playerYPos[i] = player.getyPos();
            }
        }
        Arrays.sort(allp, 0, playerCount, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return playerYPos[o1] < playerYPos[o2] ? -1 : 1;
            }
        });

        // Real draw part.
        for (int i = 0; i < playerCount; i++) {
            gc.getPlayerById(allp[i]).drawBgColor(baseCanvas, baseRealCanvas, baseColorbp[allp[i]]);
        }

        Canvas canvas = null;
        try {
            canvas = myholder.lockCanvas();
            Player myPlayer = gc.getPlayerById(gc.playerId);
            canvas.drawColor(Color.rgb(153, 153, 153));
            canvas.save();
            if (myPlayer.getEffect() == Effect.SMOKED) {
                // Smoked bomb;
                Path path = new Path();
                double x = myPlayer.getxPos();
                double y = myPlayer.getyPos();
                RectF rect = new RectF((float) ((x - ItemSmoke.viewRadius) * Global.widthScale),
                        (float) ((y - ItemSmoke.viewRadius) * Global.heightScale),
                        (float) ((x + ItemSmoke.viewRadius) * Global.widthScale),
                        (float) ((y + ItemSmoke.viewRadius) * Global.heightScale));
                path.addOval(rect, Path.Direction.CW);
                canvas.clipPath(path, Region.Op.INTERSECT);
            }
            if (graphicsQuality == 0) {
                canvas.drawBitmap(baseRealbp, scaleMatrix, null);
            } else {
                canvas.drawBitmap(basebp, scaleMatrix, null);
            }
            boolean itemDrawed = false;
            Item item = gc.getCurrentItem();
            int itemXPos = gc.getItemXPos();
            int itemYPos = gc.getItemYPos();
            if (item == null)
                itemDrawed = true;
            for (int i = 0; i < playerCount; i++) {
                Player player = gc.getPlayerById(allp[i]);
                if (!itemDrawed && player.getyPos() >= itemYPos) {
                    item.draw(canvas, itemXPos, itemYPos);
                    itemDrawed = true;
                }
                player.draw(canvas);
            }
            if (!itemDrawed) {
                item.draw(canvas, itemXPos, itemYPos);
                itemDrawed = true;
            }
            if (myPlayer.getEffect() == Effect.SMOKED) {
                double x = myPlayer.getxPos();
                double y = myPlayer.getyPos();
                RectF rect = new RectF(
                        (float) ((x - ItemSmoke.viewRadius - 5) * Global.widthScale), (float) ((y
                                - ItemSmoke.viewRadius - 5) * Global.heightScale), (float) ((x
                                + ItemSmoke.viewRadius + 5) * Global.widthScale), (float) ((y
                                + ItemSmoke.viewRadius + 5) * Global.heightScale));
                Rect rect2 = new Rect(0, 0, smokebp.getWidth() - 1, smokebp.getHeight() - 1);
                canvas.drawBitmap(smokebp, rect2, rect, null);
            }
            canvas.restore();

            String timeText = String.format("Time: %.1f", gc.getLeftTime());
            DrawUtils.drawTextLeftAlign(canvas, timeText, textPaint, 10, 10);

            // Draw "got item" message if necessary
            if (gotItemName != null) {
                if (!gc.isPausing())
                    gotItemName = null;
                else {
                    // TODO maybe a picture on top just like the original Battle
                    // Painter?
                    String name = gc.getPlayerById(gotItemPlayerId).getName();
                    Paint namePaint = getPlayerNamePaint(gotItemPlayerId, 0.7);
                    Paint strokePaint = getPlayerNameStrokePaint(0.7);
                    Paint paint = getTextPaint(0.7);

                    DrawUtils.drawTextsCenterAlignBase(canvas, new String[] { name, "got",
                            gotItemName }, new Paint[][] { { namePaint, strokePaint }, { paint },
                            { paint } }, 320, 25);
                }
            }
            // Draw Countdown Timer at the beginning
            if (countdownNumber < countdownMessage.length) {
                if (!gc.isPausing()) {
                    countdownNumber++;
                    if (countdownNumber < countdownMessage.length)
                        gc.pause(1000);
                }
                if (countdownNumber < countdownMessage.length) {
                    Paint paint = getTextPaint(1.5);
                    DrawUtils
                            .drawTextCenterScreen(canvas, countdownMessage[countdownNumber], paint);
                }
            }

        } catch (Exception ex) {
        } finally {
            if (canvas != null)
                myholder.unlockCanvasAndPost(canvas);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // Nothing to do.
    }

    private void initializeBitmap() {
        Effect.initialize(this);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        // Bitmaps
        Bitmap arrowbp;
        Bitmap arrowfastbp;
        Bitmap shadowbp;

        arrowbp = BitmapFactory.decodeResource(getResources(), R.drawable.circle, options);
        arrowfastbp = BitmapFactory.decodeResource(getResources(), R.drawable.circlefast, options);
        shadowbp = BitmapFactory.decodeResource(getResources(), R.drawable.shadow, options);
        smokebp = BitmapFactory.decodeResource(getResources(), R.drawable.effect_smoke, options);

        for (int i = 0; i < 4; i++) {
            Bitmap normalbp[] = new Bitmap[2];
            Bitmap fastbp[] = new Bitmap[2];
            Bitmap flyingbp[] = new Bitmap[31];
            for (int j = 0; j < 2; j++) {
                // TODO dynamic package name
                normalbp[j] = BitmapFactory.decodeResource(
                        getResources(),
                        getResources().getIdentifier(String.format("normal%d_%d", i + 1, j + 1),
                                "drawable", "tw.nmlab.piquok"), options);
            }
            for (int j = 0; j < 2; j++) {
                // TODO dynamic package name
                fastbp[j] = BitmapFactory.decodeResource(
                        getResources(),
                        getResources().getIdentifier(String.format("fast%d_%d", i + 1, j + 1),
                                "drawable", "tw.nmlab.piquok"), options);
            }
            for (int j = 0; j < 31; j++) {
                // TODO dynamic package name
                flyingbp[j] = BitmapFactory.decodeResource(
                        getResources(),
                        getResources().getIdentifier(String.format("fly%d_%d", i + 1, j + 1),
                                "drawable", "tw.nmlab.piquok"), options);
            }

            AnimatedSprite normalsp = new AnimatedSprite();
            normalsp.Initialize(normalbp, 5.0);
            AnimatedSprite fastsp = new AnimatedSprite();
            fastsp.Initialize(fastbp, 5.0);
            AnimatedSprite flysp = new AnimatedSprite();
            flysp.Initialize(flyingbp, 20.0);

            gc.getPlayerById(i).setSprites(normalsp, fastsp, flysp, flysp, arrowbp, arrowfastbp,
                    shadowbp);
        }

        basebp = Bitmap.createBitmap(Global.baseWindowWidth, Global.baseWindowHeight,
                Bitmap.Config.ARGB_8888);
        baseCanvas = new Canvas(basebp);
        baseCanvas.drawColor(Color.WHITE);

        if (graphicsQuality == 0) {
            baseRealbp = Bitmap.createBitmap(Global.baseWindowWidth, Global.baseWindowHeight,
                    Bitmap.Config.ARGB_8888);
            Bitmap base = BitmapFactory.decodeResource(getResources(), R.drawable.base, options);
            baseRealCanvas = new Canvas(baseRealbp);
            baseRealCanvas.drawBitmap(base, new Matrix(), null);
            baseColorbp = new Bitmap[Global.maxPlayer];
            for (int i = 0; i < Global.maxPlayer; i++) {
                baseColorbp[i] = BitmapFactory.decodeResource(
                        getResources(),
                        getResources().getIdentifier(String.format("base%d", i + 1), "drawable",
                                "tw.nmlab.piquok"), options);
            }
        } else {
            baseRealbp = null;
            baseRealCanvas = null;
            baseColorbp = new Bitmap[Global.maxPlayer];
            for (int i = 0; i < Global.maxPlayer; i++) {
                baseColorbp[i] = null;
            }
        }
    }

    private Thread gameThread;

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        gc.initialize();
        initializeBitmap();
        eventCountdownTimer();
        gameThread = new Thread(parent);
        gameThread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // Nothing to do.
    }

    private Paint getTextPaint(double scale) {
        Paint paint = new Paint();
        paint.setARGB(255, 32, 32, 32);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize((int) (bigFontSize * Global.heightScale * scale));
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setAntiAlias(true);
        return paint;
    }

    private Paint getPlayerNamePaint(int playerid, double scale) {
        Paint namePaint = new Paint();
        namePaint.setColor(GameController.getPlayerColorById(playerid));
        namePaint.setStyle(Paint.Style.FILL);
        namePaint.setTextSize((int) (bigFontSize * Global.heightScale * scale));
        namePaint.setTypeface(Typeface.DEFAULT_BOLD);
        namePaint.setAntiAlias(true);
        return namePaint;
    }

    private Paint getPlayerNameStrokePaint(double scale) {
        Paint strokePaint = new Paint();
        strokePaint.setARGB(255, 32, 32, 32);
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setTextSize((int) (bigFontSize * Global.heightScale * scale));
        strokePaint.setTypeface(Typeface.DEFAULT_BOLD);
        strokePaint.setAntiAlias(true);
        strokePaint.setStrokeWidth(1);
        return strokePaint;
    }

    private boolean isFinalScreen = false;

    public void drawEndGame() {
        Log.i(TAG, "End game.");
        int[] count = gc.countColors();
        if (Global.serverConnection != null) {
            String[] str = new String[Global.maxPlayer];
            for (int i = 0; i < Global.maxPlayer; i++) {
                str[i] = String.valueOf(count[i]);
            }
            Global.serverConnection.broadcast(Global.RESPONSE_GAME_ENDED, str);
        }
        int total = Global.baseWindowWidth * Global.baseWindowHeight;
        for (int i = 0; i < Global.maxPlayer; i++) {
            Log.i(TAG, String.format("Player %d: %f%%", i + 1, (double) count[i] / total * 100.0));
            count[i] = (int) ((double) count[i] / total * 1000.0);
        }

        Paint paint = getTextPaint(1.0);

        Canvas canvas = null;

        for (int i = -1; i <= Global.maxPlayer; i++) {
            // i == maxPlayer is for win message.
            if (i >= 0 && i < Global.maxPlayer && !gc.getPlayerById(i).isAvailable())
                continue;
            try {
                canvas = myholder.lockCanvas();
                canvas.drawColor(Color.WHITE);
                DrawUtils.drawTextCenterAlign(canvas, "Result", paint, 40);
                for (int j = 0; j <= i; j++) {
                    if (j < Global.maxPlayer) {
                        Player player2 = gc.getPlayerById(j);
                        if (!player2.isAvailable())
                            continue;
                        String playerName = player2.getName();
                        Paint namePaint = getPlayerNamePaint(j, 0.8);
                        Paint strokePaint = getPlayerNameStrokePaint(0.8);

                        DrawUtils.drawTextRightAlign(canvas, playerName, namePaint,
                                Global.baseWindowWidth * 0.5 - 30, 115 + 65 * j);
                        DrawUtils.drawTextRightAlign(canvas, playerName, strokePaint,
                                Global.baseWindowWidth * 0.5 - 30, 115 + 65 * j);

                        String percentText = String.format("%-4.1f%%", count[j] / 10.0);
                        paint.setARGB(255, 32, 32, 96);

                        DrawUtils.drawTextLeftAlign(canvas, percentText, paint,
                                Global.baseWindowWidth * 0.5 + 30, 115 + 65 * j);

                        paint.setARGB(255, 32, 32, 32);
                    } else {
                        int maxCount = 0;
                        for (int q = 0; q < Global.maxPlayer; q++) {
                            maxCount = Math.max(maxCount, count[q]);
                        }
                        int pi = -1;
                        for (int q = 0; q < Global.maxPlayer; q++) {
                            if (count[q] == maxCount) {
                                if (pi == -1)
                                    pi = q;
                                else
                                    pi = -2;
                            }
                        }
                        String winText;
                        if (pi == -2)
                            winText = "Tie.";
                        else
                            winText = String.format("%s wins!", gc.getPlayerById(pi).getName());
                        DrawUtils.drawTextCenterAlign(canvas, winText, paint, 380);
                        Paint smallTextPaint = getTextPaint(0.3);
                        smallTextPaint.setARGB(255, 128, 128, 128);
                        DrawUtils.drawTextRightAlignBase(canvas, "touch screen to continue...",
                                smallTextPaint, 780, 460);
                    }
                }
            } catch (Exception ex) {
            } finally {
                if (canvas != null) {
                    myholder.unlockCanvasAndPost(canvas);
                    canvas = null;
                }
            }
            try {
                if (i == Global.maxPlayer) {
                    isFinalScreen = true;
                    Thread.sleep(3600000);
                } else
                    Thread.sleep(750);
            } catch (InterruptedException e) {
            } finally {
                isFinalScreen = false;
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isFinalScreen && event.getAction() == MotionEvent.ACTION_DOWN) {
            gameThread.interrupt();
            return true;
        }
        return gc.onTouchEvent(event);
    }
}
