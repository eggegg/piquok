package tw.nmlab.piquok.game;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.R;
import tw.nmlab.piquok.Utils;
import tw.nmlab.piquok.game.Player.PlayerType;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.MotionEvent;

public class ServerGameController extends GameController {

    private static final String TAG = "ServerGameController";

    // Item data
    private double lastItemSpawnTime = 0.0;
    private double itemSpawnRate = 10.0;

    @Override
    public void setItemSpawnRate(double itemSpawnRate) {
        this.itemSpawnRate = itemSpawnRate;
    }

    private final double itemNoSpawnRange = 180;
    private int totalClient;

    public ServerGameController(GameActivity parent, int totalClient) {
        this.parent = parent;
        this.totalClient = totalClient;
    }

    private final int itemNoSpawnEdgeWidth = 50;

    private void spawnItem() {
        if (currentItem != null)
            currentItem = null;
        currentItem = allItems[Utils.randInt(0, allItems.length - 1)];
        currentItem.reset();
        boolean flag;
        do {
            itemXPos = Utils.randInt(itemNoSpawnEdgeWidth, Global.baseWindowWidth - 1
                    - itemNoSpawnEdgeWidth);
            itemYPos = Utils.randInt(itemNoSpawnEdgeWidth, Global.baseWindowHeight - 1
                    - itemNoSpawnEdgeWidth);
            flag = false;
            for (int i = 0; i < Global.maxPlayer; i++) {
                if (player[i].isAvailable()
                        && Math.hypot(player[i].getxPos() - itemXPos, player[i].getyPos()
                                - itemYPos) <= itemNoSpawnRange) {
                    flag = true;
                    break;
                }
            }
        } while (flag);
        Log.i(TAG, String.format("Item %s spawned at %d, %d", currentItem.getName(), itemXPos,
                itemYPos));

        if (Global.serverConnection != null)
            Global.serverConnection.broadcast(Global.RESPONSE_ITEM_INFO,
                    new String[] { String.valueOf(currentItem.getId()), String.valueOf(itemXPos),
                            String.valueOf(itemYPos) });
    }

    @Override
    public void update() {
        if (!gameStarted) {
            // Don't include self.
            if (Global.loadedClient == totalClient - 1) {
                gameStarted = true;
                if (Global.serverConnection != null)
                    Global.serverConnection.broadcast(Global.RESPONSE_GAME_LOADED, null);
            } else {
                return;
            }
        }
        if (updatePause())
            return;
        updateNumber++;
        if (Global.serverConnection != null)
            Global.serverConnection.broadcast(Global.RESPONSE_GAME_TIME,
                    new String[] { String.valueOf(updateNumber) });
        // Game time ends.
        if (getLeftTime() <= 0) {
            parent.flagGamePlaying = false;
            isGameCompleted = true;
        }

        // Spawn item.
        if (getElapsedTime() > lastItemSpawnTime + itemSpawnRate) {
            lastItemSpawnTime = getElapsedTime();
            spawnItem();
        }

        // Update positions.
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (player[i].isAvailable())
                player[i].update();
        }

        // Update item
        if (currentItem != null)
            currentItem.update();

        // Do collisions.
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (!player[i].isAvailable() || player[i].isFlying)
                continue;
            if (currentItem != null && player[i].isCollide(currentItem, itemXPos, itemYPos)) {
                musicPlayer = MediaPlayer.create(parent, R.raw.prize);
                musicPlayer.start();
                currentItem.affect(this, myView, i);
                if (Global.serverConnection != null)
                    Global.serverConnection
                            .broadcast(
                                    Global.RESPONSE_PLAYER_GOT_ITEM,
                                    new String[] { String.valueOf(i),
                                            String.valueOf(currentItem.getId()) });
                myView.eventPlayerGotItem(i, currentItem);
                currentItem = null;
                continue;
            }
            for (int j = i + 1; j < Global.maxPlayer; j++) {
                if (!player[j].isAvailable() || player[j].isFlying)
                    continue;
                if (player[i].isCollide(player[j])) {
                    musicPlayer = MediaPlayer.create(parent, R.raw.bounce);
                    musicPlayer.start();
                    if (Global.serverConnection != null)
                        Global.serverConnection.broadcast(Global.RESPONSE_PLAYER_BOUNCE, null);
                    player[i].updateBumpDirection(player[j]);
                    player[j].updateBumpDirection(player[i]);
                    player[i].startFlying();
                    player[j].startFlying();
                    break;
                }
            }
        }

        // Update AI
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (player[i].getType() == PlayerType.AI) {
                player[i].updateAI();
            }
        }

        for (int i = 0; i < Global.maxPlayer; i++) {
            if (player[i].isAvailable()) {
                player[i].broadcast();
            }
        }
    }

    public int[] countColors() {
        Bitmap basebp = myView.getBasebp();
        int total = basebp.getWidth() * basebp.getHeight();
        int finalColor[] = new int[total];
        basebp.getPixels(finalColor, 0, basebp.getWidth(), 0, 0, basebp.getWidth(),
                basebp.getHeight());
        int count[] = new int[Global.maxPlayer];
        for (int i = 0; i < total; i++) {
            for (int j = 0; j < Global.maxPlayer; j++) {
                if (finalColor[i] == playerColor[j]) {
                    count[j]++;
                    break;
                }
            }
        }
        return count;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void changeMode(int playerId) {
        if (player[playerId] == null) {
            return;
        }
        if (player[playerId].getType() != PlayerType.PLAYER) {
            Log.e(TAG, "Got weird changeMode request.");
            return;
        }
        player[playerId].setMode(1 - player[playerId].getMode());
    }

    public void setDirection(int playerId, int direction) {
        if (player[playerId].getType() != PlayerType.PLAYER) {
            Log.e(TAG, "Got weird setDirection request.");
            return;
        }
        player[playerId].setDirection(direction);
    }

    @Override
    protected void onShake() {
        if (!parent.flagGamePlaying) {
            return;
        }
        changeMode(playerId);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!parent.flagGamePlaying) {
            return false;
        }
        if (player[playerId].getType() != PlayerType.PLAYER) {
            return false;
        }
        // 0: forward, -1: turn right, 1: turn left
        double x = event.getX();
        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN:
        case MotionEvent.ACTION_MOVE:
            if (x <= Global.windowWidth / 2) {
                setDirection(playerId, 1);
            } else {
                setDirection(playerId, -1);
            }
            break;
        case MotionEvent.ACTION_UP:
            setDirection(playerId, 0);
            break;
        }
        return true;
    }

    public void playerExitGame(String playerName) {
        int place = -1;
        for (int i = 0; i < Global.maxPlayer; i++) {
            if (player[i].isAvailable() && player[i].getName().equals(playerName)) {
                place = i;
                break;
            }
        }
        if (place == -1) {
            Log.e(TAG, String.format("player %s exited but I don't know who it is...", playerName));
            return;
        }
        if (player[place].getType() == PlayerType.AI) {
            Log.e(TAG, String.format("Computer %s exited game???", playerName));
            return;
        }
        parent.makeToast(String.format("%s had disconnected, and is controlled by AI now.",
                player[place].getName()));
        player[place].setType(PlayerType.AI);
        if (Global.serverConnection != null) {
            Global.serverConnection.broadcast(Global.RESPONSE_PLAYER_DISCONNECTED,
                    new String[] { String.valueOf(place) });
        }
    }

}
