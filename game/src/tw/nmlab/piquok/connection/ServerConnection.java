package tw.nmlab.piquok.connection;

import java.io.IOException;
import java.io.PipedOutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.game.ServerGameController;
import tw.nmlab.piquok.lobby.ServerLobbyController;
import tw.nmlab.piquok.parser.ServerParser;
import android.util.Log;

/**
 * Listener for TCP sockets
 * 
 * @author eggegg
 */
public class ServerConnection extends Thread {
    /** Tag string */
    private final static String TAG = "ServerConnection";
    /** Listen selector */
    private Selector selector;
    /** Shared server socket */
    private ServerSocketChannel channel;
    /** Pre-allocate buffer */
    private ByteBuffer buffer = ByteBuffer.allocate(8192);
    /** Pending changes */
    private final HashMap<SocketChannel, LinkedList<ByteBuffer>> pending = new HashMap<SocketChannel, LinkedList<ByteBuffer>>();
    /** Flag to stop the thread */
    private boolean endFlag;

    private ServerGameController gameController = null;
    private ServerLobbyController lobbyController = null;
    private int connectId = 0;
    private HashMap<SocketChannel, Integer> connectionMap = new HashMap<SocketChannel, Integer>();
    private HashMap<Integer, SocketChannel> idMap = new HashMap<Integer, SocketChannel>();
    private HashMap<SocketChannel, PipedOutputStream> outputMap = new HashMap<SocketChannel, PipedOutputStream>();
    private LinkedList<ServerParser> parserList = new LinkedList<ServerParser>();

    /**
     * Initialize a TCP socket listener
     * 
     * @throws IOException
     *             if the port cannot be bind
     */
    public ServerConnection(int port) throws IOException {
        selector = SelectorProvider.provider().openSelector();
        channel = ServerSocketChannel.open();

        InetSocketAddress isa = new InetSocketAddress((InetAddress) null, port);
        channel.socket().bind(isa);
        channel.configureBlocking(false);
        channel.register(selector, SelectionKey.OP_ACCEPT);
    }

    /** Thread for catching connection */
    @Override
    public void run() {
        while (true) {
            try {
                // wait the connection
                selector.select();

                // terminate the thread
                if (endFlag) {
                    selector.close();
                    channel.close();
                    return;
                }

                Iterator<SelectionKey> it = selector.selectedKeys().iterator();
                while (it.hasNext()) {
                    SelectionKey key = it.next();
                    it.remove();
                    if (key.isValid() && key.isAcceptable()) {
                        Log.d(TAG, "Connection came in");
                        accept(key);
                    }
                    if (key.isValid() && key.isReadable()) {
                        read(key);
                    }
                    if (key.isValid() && key.isWritable()) {
                        write(key);
                    }
                }
            } catch (IOException e) {
                Log.e(TAG, "Connection failed");
            }
        }
    }

    /** Stop the thread */
    public synchronized void end() {
        endFlag = true;
        for (Map.Entry<SocketChannel, PipedOutputStream> it : outputMap.entrySet()) {
            PipedOutputStream ostream = it.getValue();
            try {
                ostream.close();
            } catch (IOException e) {
            }
            SocketChannel channel = it.getKey();
            try {
                channel.close();
            } catch (IOException e) {
            }
        }
        selector.wakeup();
    }

    /** Callback when a connection came in */
    private void accept(SelectionKey key) throws IOException {
        // For an accept to be pending the channel must be a server socket
        // channel.
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();

        // Accept the connection and make it non-blocking
        SocketChannel socketChannel = serverSocketChannel.accept();
        if (socketChannel == null)
            return;
        socketChannel.configureBlocking(false);

        // Register the new SocketChannel with our Selector, indicating
        // we'd like to be notified when there's data waiting to be read
        socketChannel.register(selector, SelectionKey.OP_READ);

        onAccept(socketChannel);
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        // Clear out our read buffer so it's ready for new data
        buffer.clear();

        // Attempt to read off the channel
        int size;
        try {
            size = socketChannel.read(buffer);
        } catch (IOException e) {
            // The remote forcibly closed the connection, cancel
            // the selection key and close the channel.
            key.cancel();
            outputMap.get(socketChannel).close();
            socketChannel.close();
            return;
        }

        if (size == -1) {
            // Remote entity shut the socket down cleanly. Do the
            // same from our end and cancel the channel.
            key.cancel();
            outputMap.get(socketChannel).close();
            socketChannel.close();
            return;
        }

        // Handle the response
        onRead(socketChannel, buffer.array(), size);
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        if (!socketChannel.isOpen()) {
            return;
        }

        // Write until there's not more data ...
        synchronized (pending) {
            LinkedList<ByteBuffer> queue = pending.get(socketChannel);
            while (queue != null && !queue.isEmpty()) {
                ByteBuffer buf = queue.getFirst();
                try {
                    socketChannel.write(buf);
                } catch (IOException ex) {
                    // The remote forcibly closed the connection, cancel
                    // the selection key and close the channel.
                    key.cancel();
                    outputMap.get(socketChannel).close();
                    socketChannel.close();
                    return;
                }
                if (buf.remaining() > 0) {
                    System.err.println("remaining > 0");
                    return;
                }
                queue.removeFirst();
            }
            key.interestOps(SelectionKey.OP_READ);
        }
    }

    /**
     * Send data through the socket
     * 
     * @param buf
     *            data array
     */
    public void send(SocketChannel channel, byte[] buf, int offset, int length) throws IOException {
        if (!pending.containsKey(channel)) {
            pending.put(channel, new LinkedList<ByteBuffer>());
        }
        LinkedList<ByteBuffer> queue = pending.get(channel);

        ByteBuffer bb = ByteBuffer.allocate(length);
        bb.put(buf, offset, length);
        bb.rewind();
        synchronized (pending) {
            queue.add(bb);
        }
        channel.register(selector, SelectionKey.OP_WRITE);
        selector.wakeup();
    }

    public void send(SocketChannel channel, byte[] buf) throws IOException {
        send(channel, buf, 0, buf.length);
    }

    // Only used here.
    private void send(SocketChannel channel, String str) {
        try {
            send(channel, str.getBytes());
        } catch (IOException e) {
        }
    }

    public void send(int channelId, String str) throws IOException {
        send(idMap.get(channelId), str.getBytes());
    }

    public void send(int channelId, int key, String[] str) {
        StringBuilder sb = new StringBuilder();
        sb.append(key);
        if (str != null) {
            for (int i = 0; i < str.length; i++) {
                sb.append(' ');
                sb.append(str[i]);
            }
        }
        sb.append(Global.stringSeparator);
        try {
            send(channelId, sb.toString());
        } catch (IOException e) {
        }
    }

    public void broadcast(int key, String[] str) {
        StringBuilder sb = new StringBuilder();
        sb.append(key);
        if (str != null) {
            for (int i = 0; i < str.length; i++) {
                sb.append(' ');
                sb.append(str[i]);
            }
        }
        sb.append(Global.stringSeparator);
        for (int channelId : idMap.keySet()) {
            try {
                send(channelId, sb.toString());
            } catch (IOException e) {
            }
        }
    }

    public void registerLobbyController(ServerLobbyController control) {
        lobbyController = control;
        synchronized (parserList) {
            for (ServerParser parser : parserList) {
                parser.registerLobbyController(control);
            }
        }
    }

    public void registerGameController(ServerGameController control) {
        gameController = control;
        synchronized (parserList) {
            for (ServerParser parser : parserList) {
                parser.registerGameController(control);
            }
        }
    }

    private void onAccept(SocketChannel channel) {
        if (gameController != null) {
            Global.serverConnection.send(channel,
                    String.valueOf(Global.RESPONSE_GAME_ALREADY_START));
        } else if (lobbyController != null) {
            int id = connectId++;
            PipedOutputStream ostream = new PipedOutputStream();
            ServerParser parser = new ServerParser(ostream, this);

            synchronized (parserList) {
                connectionMap.put(channel, id);
                idMap.put(id, channel);
                outputMap.put(channel, ostream);
                parserList.add(parser);
            }
            parser.setConnectionId(id);
            parser.registerGameController(gameController);
            parser.registerLobbyController(lobbyController);

            parser.start();
        } else {
            Log.e(TAG, "Accepted strange connection");
        }
    }

    private void onRead(SocketChannel channel, byte[] data, int size) {
        if (gameController != null || lobbyController != null) {
            PipedOutputStream ostream = outputMap.get(channel);
            if (ostream == null)
                return;
            try {
                ostream.write(data, 0, size);
            } catch (IOException e) {
            }
        } else {
            Log.e(TAG, "Read from strange connection");
        }
    }

    public void disconnect(ServerParser parser) {
        int id = parser.getConnectionId();
        SocketChannel channel = idMap.get(id);
        synchronized (parserList) {
            connectionMap.remove(channel);
            idMap.remove(id);
            outputMap.remove(channel);
            parserList.remove(parser);
            pending.remove(channel);
        }
        try {
            channel.close();
        } catch (IOException e) {
        }
        Log.i(TAG, String.format("Disconnected %d", id));
    }
}
