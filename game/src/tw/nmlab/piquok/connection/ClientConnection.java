package tw.nmlab.piquok.connection;

import java.io.IOException;
import java.io.PipedOutputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.LinkedList;

import tw.nmlab.piquok.Global;
import tw.nmlab.piquok.game.ClientGameController;
import tw.nmlab.piquok.lobby.ClientLobbyController;
import tw.nmlab.piquok.parser.ClientParser;
import android.util.Log;

/**
 * Non-blocking connection class
 * 
 * @author eggegg
 */
public class ClientConnection extends Thread {
    /** Tag */
    private static final String TAG = "ClientConnection";
    /** Socket timeout */
    private static final int timeout = 3000;
    /** Unique id */
    private static int globalId = 0;

    /**
     * Generate unique id
     * 
     * @return id in integer
     */
    private static synchronized int newConnectionId() {
        return globalId++;
    }

    /** Selector object */
    private Selector selector;
    /** Socket object */
    private SocketChannel channel;
    /** Pre-allocate buffer */
    private ByteBuffer buffer = ByteBuffer.allocate(8192);
    /** Pending changes */
    private final LinkedList<ByteBuffer> pending = new LinkedList<ByteBuffer>();
    /** Id of the connection */
    private int id;
    /** End flag for stopping the connection */
    private boolean endFlag = false;

    private ClientParser parser;
    private PipedOutputStream ostream;

    /**
     * Initialize an object from a new socket connection for server
     * 
     * @param socket
     *            socket from dispatcher
     * @throws IOException
     *             connection failure
     */
    public ClientConnection(SocketChannel income) throws IOException {
        setupChannel(income);
    }

    /**
     * Initialize a connection connecting to server
     * 
     * @param server
     *            server name
     * @param port
     *            port number
     * @throws IOException
     *             connection failure
     */
    public ClientConnection(String server, int port) throws IOException {
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.configureBlocking(false);
        socketChannel.connect(new InetSocketAddress(server, port));
        setupChannel(socketChannel);
        final int sleepInterval = 100;
        int connectTime = 0;
        while (!socketChannel.finishConnect() && connectTime <= timeout) {
            connectTime += sleepInterval;
            // Some hack to ensure that we're connected. Other solution?
            try {
                sleep(sleepInterval);
            } catch (InterruptedException ex) {
                Log.e(TAG, "Cannot initialize socket");
            }
        }

        ostream = new PipedOutputStream();
        parser = new ClientParser(ostream);
        parser.start();
    }

    private void setupChannel(SocketChannel income) throws IOException {
        id = newConnectionId();
        channel = income;

        selector = SelectorProvider.provider().openSelector();
        channel.socket().setSoTimeout(timeout);
        channel.register(selector, SelectionKey.OP_READ);
    }

    /**
     * Getting unique id represents the connection
     * 
     * @return id number
     */
    public int getConnectionId() {
        return id;
    }

    /** Thread runner */
    @Override
    public void run() {
        try {
            while (!selector.keys().isEmpty()) {
                selector.select();
                // terminate the thread
                if (endFlag && pending.isEmpty()) {
                    channel.close();
                    selector.close();
                    Log.d(TAG, "end by endflag = true");
                    break;
                }

                for (SelectionKey key : selector.selectedKeys()) {
                    if (key.isValid() && key.isReadable()) {
                        read(key);
                    }
                    // key may become invalid after read(key);
                    if (key.isValid() && key.isWritable()) {
                        write(key);
                    }
                }
            }
            Log.d(TAG, "Connection run end.");
        } catch (IOException ex) {
            Log.e(TAG, "Connection failed");
        }
    }

    /** Stop the thread */
    public void end() {
        endFlag = true;
        try {
            ostream.close();
        } catch (IOException e) {
        }
        selector.wakeup();
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        // Clear out our read buffer so it's ready for new data
        buffer.clear();

        // Attempt to read off the channel
        int size;
        try {
            size = socketChannel.read(buffer);
        } catch (IOException e) {
            // The remote forcibly closed the connection, cancel
            // the selection key and close the channel.
            key.cancel();
            socketChannel.close();
            end();
            return;
        }

        if (size == -1) {
            // Remote entity shut the socket down cleanly. Do the
            // same from our end and cancel the channel.
            key.cancel();
            socketChannel.close();
            end();
            return;
        }

        // Handle the response
        onRead(buffer.array(), size);
    }

    public void send(byte[] buf, int offset, int length) throws IOException {
        ByteBuffer bb = ByteBuffer.allocate(length);
        bb.put(buf, offset, length);
        bb.rewind();
        synchronized (pending) {
            pending.add(bb);
        }
        channel.register(selector, SelectionKey.OP_WRITE);
        selector.wakeup();
    }

    public void send(byte[] buf) throws IOException {
        send(buf, 0, buf.length);
    }

    public void send(String str) throws IOException {
        Log.d(TAG, str);
        send(str.getBytes());
    }

    public void send(int key, String[] str) {
        StringBuilder sb = new StringBuilder();
        sb.append(key);
        if (str != null) {
            for (int i = 0; i < str.length; i++) {
                sb.append(' ');
                sb.append(str[i]);
            }
        }
        sb.append(Global.stringSeparator);
        try {
            send(sb.toString());
        } catch (IOException e) {
        }
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        if (!socketChannel.isOpen()) {
            return;
        }

        // Write until there's not more data ...
        synchronized (pending) {
            while (!pending.isEmpty()) {
                ByteBuffer buf = pending.getFirst();
                try {
                    socketChannel.write(buf);
                } catch (IOException ex) {
                    // The remote forcibly closed the connection, cancel
                    // the selection key and close the channel.
                    key.cancel();
                    socketChannel.close();
                    end();
                    return;
                }
                if (buf.remaining() > 0) {
                    System.err.println("remaining > 0");
                    return;
                }
                pending.removeFirst();
            }
            key.interestOps(SelectionKey.OP_READ);
        }
    }

    public void registerLobbyController(ClientLobbyController control) {
        parser.registerLobbyController(control);
    }

    public void registerGameController(ClientGameController control) {
        parser.registerGameController(control);
    }

    private void onRead(byte[] data, int size) {
        try {
            ostream.write(data, 0, size);
        } catch (IOException e) {
        }
    }
}
