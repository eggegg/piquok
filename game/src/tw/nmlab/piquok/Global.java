package tw.nmlab.piquok;

import tw.nmlab.piquok.connection.ClientConnection;
import tw.nmlab.piquok.connection.ServerConnection;
import android.util.DisplayMetrics;

public class Global {
    public static final int serverPort = 3535;

    public static int windowWidth;
    public static int windowHeight;
    public static double heightScale;
    public static double widthScale;

    // all calculate is based on this coordinate.
    public static final int baseWindowWidth = 800, baseWindowHeight = 480;

    public static void initialize(DisplayMetrics dm) {
        windowWidth = dm.widthPixels;
        windowHeight = dm.heightPixels;
        widthScale = (double) windowWidth / baseWindowWidth;
        heightScale = (double) windowHeight / baseWindowHeight;
    }

    public static String myName;

    public static final int maxPlayer = 4;
    public static final int frameInterval = 50; // FPS = 20.

    public static ServerConnection serverConnection = null;
    public static ClientConnection clientConnection = null;

    public static final char stringSeparator = '\7';

    // Server
    public static int loadedClient;

    // Connection key constants.

    // Lobby C2S
    public static final int ACTION_TEST = 0;
    public static final int ACTION_JOIN_GAME = 1;
    public static final int ACTION_CHANGE_PLACE = 2;
    public static final int ACTION_CHANGE_READY = 3;

    // Lobby S2C
    public static final int RESPONSE_INVALID_NAME = 100;
    public static final int RESPONSE_FULL_GAME = 101;
    public static final int RESPONSE_GAME_JOINED = 102;
    public static final int RESPONSE_CONNECTED = 103;
    public static final int RESPONSE_GAME_PLAYER = 104;
    public static final int RESPONSE_GAME_INFO = 105;
    public static final int RESPONSE_GAME_EXITED = 107;
    public static final int RESPONSE_PLACE_CHANGE = 108;
    public static final int RESPONSE_READY_CHANGE = 109;
    public static final int RESPONSE_GAME_START = 110;

    // Game C2S
    public static final int ACTION_CHANGE_MODE = 200;
    public static final int ACTION_CHANGE_DIRECTION = 201;

    // Game S2C
    public static final int RESPONSE_GAME_LOADED = 300;
    public static final int RESPONSE_PLAYER_INFO = 301;
    public static final int RESPONSE_PLAYER_START_FLY = 302;
    public static final int RESPONSE_ITEM_INFO = 303;
    public static final int RESPONSE_PLAYER_GOT_ITEM = 304;
    public static final int RESPONSE_GAME_ENDED = 305;
    public static final int RESPONSE_GAME_TIME = 306;
    public static final int RESPONSE_PLAYER_DISCONNECTED = 307;
    public static final int RESPONSE_PLAYER_BOUNCE = 308;
    
    public static final int RESPONSE_GAME_ALREADY_START = 400;
}
