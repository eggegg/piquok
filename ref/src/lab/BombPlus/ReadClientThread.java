package lab.BombPlus;

import java.io.IOException;

import android.os.Handler;
import android.os.Message;

public class ReadClientThread implements Runnable {
	int cId;
	Handler gameHandler;
	ServerAgent msa = BombHostActivity.mServerAgent;
	boolean flagServerReading = true;

	ReadClientThread(int inId, Handler inHandler) {
		cId = inId;
		gameHandler = inHandler;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (flagServerReading) {
			try {

				String tmp = msa.readFromId(cId);
				if (tmp != null) {
					if (tmp.equals("bombcome")) {

						Message m = new Message();
						m.what = 0x101;
						m.arg1 = cId;
						m.arg2 = Integer.parseInt(msa.readFromId(cId));
						gameHandler.sendMessage(m);
					} else if (tmp.equals("senddeg")) {
						Message m = new Message();
						m.what = 0x102;
						m.arg1 = cId;
						m.arg2 = Integer.parseInt(msa.readFromId(cId));
						gameHandler.sendMessage(m);
					} else if (tmp.equals("gameleft")) {
						Message m = new Message();
						m.what = 0x107;
						m.arg1 = cId;
						gameHandler.sendMessage(m);
					} else if (tmp.equals("gameover")) {

						msa.writeAll("gameover");
						flagServerReading = false;

					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
