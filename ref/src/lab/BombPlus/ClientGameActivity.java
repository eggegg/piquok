package lab.BombPlus;

import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.app.Service;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

public class ClientGameActivity extends Activity implements
		SensorEventListener, Runnable {

	// 因為不能直接在thread中用Toast,所以使用Handler
	Handler myHandler = new Handler() {
		// this method will handle the calls from other threads.
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0x101:
				mca.write("bombcome");
				mca.write("" + msg.arg1);

				mVibrator.cancel();

				if (mMedia != null) {
					mMedia.stop();
					mMedia.reset();
				}
				break;
			case 0x104:
				mca.write("gameover");
				mVibrator.cancel();
				mVibrator.vibrate(new long[] { 0, 2000 }, -1);

				if (mMedia != null) {
					mMedia.stop();
					mMedia.reset();
				}

				mMedia = MediaPlayer.create(ClientGameActivity.this,
						R.raw.bombblow);
				try {
					mMedia.prepare();
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				mMedia.start();

				Toast.makeText(getBaseContext(), R.string.lose,
						Toast.LENGTH_SHORT).show();
				ending(false);
				break;
			case 0x105:
				mca.write("gameover");
				Toast.makeText(getBaseContext(), R.string.win,
						Toast.LENGTH_SHORT).show();
				if (mMedia != null) {
					mMedia.stop();
					mMedia.reset();
				}

				ending(true);
				break;

			}
		}
	};

	MySurfaceView mysufview;
	private SensorManager sensorManager;
	private Vibrator mVibrator;
	private float outDeg = -1;
	ClientAgent mca = NFCReceiveActivity.mClientAgent;
	int nowDeg = 0;
	Thread gameThread;
	Thread readThread;
	boolean flagGamePlaying = true;
	boolean flagReadThread = true;
	boolean flagLastTime = false;
	private MediaPlayer mMedia;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Global.flagGamePlayed = true;

		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

		DisplayMetrics dm = new DisplayMetrics();
		this.getWindowManager().getDefaultDisplay().getMetrics(dm);
		mysufview = new MySurfaceView(this, dm);

		mVibrator = (Vibrator) getApplication().getSystemService(
				Service.VIBRATOR_SERVICE);

		mMedia = MediaPlayer.create(ClientGameActivity.this, R.raw.bombslow);
		setContentView(mysufview);
		gameThread = new Thread(this);
		gameThread.start();
		readThread = new Thread() {// 從server讀字串的thread
			String tmp = "";

			@Override
			public void run() {
				while (flagReadThread) {
					try {
						tmp = mca.read();
						if (tmp == null) {
							tmp = "";
							Log.e("null", "null");
						}
						if (tmp.equals("bombcome")) {
							mysufview.bombIn(Integer.parseInt(mca.read()));

							if (mMedia != null) {
								mMedia.stop();
								mMedia.reset();
							}
							if (!flagLastTime) {

								mVibrator.vibrate(new long[] { 1000, 1000 }, 0);

								mMedia = MediaPlayer
										.create(ClientGameActivity.this,
												R.raw.bombslow);
							} else {

								mVibrator.vibrate(new long[] { 500, 500 }, 0);

								mMedia = MediaPlayer.create(
										ClientGameActivity.this,
										R.raw.bombquick);
							}
							try {
								mMedia.prepare();
							} catch (IllegalStateException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							mMedia.start();

						} else if (tmp.equals("gamelose")) {
							mca.read();// 把多的gamewin吃掉
							myHandler.sendEmptyMessage(0x104);

						} else if (tmp.equals("gamewin")) {
							myHandler.sendEmptyMessage(0x105);
						} else if (tmp.equals("lasttime")) {
							mysufview.lastTime();
							flagLastTime = true;

							if (mysufview.flagHaveBomb) {
								mVibrator.cancel();
								mVibrator.vibrate(new long[] { 500, 500 }, 0);

								if (mMedia != null) {
									mMedia.stop();
									mMedia.reset();
								}
								mMedia = MediaPlayer.create(
										ClientGameActivity.this,
										R.raw.bombquick);

								try {
									mMedia.prepare();
								} catch (IllegalStateException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								mMedia.start();
							}
						} else if (tmp.equals("gameover")) {
							flagReadThread = false;
						}
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				mca.end();
			}
		};
		readThread.start();

	}

	@Override
	protected void onResume() {
		super.onResume();
		List<Sensor> sensors = sensorManager
				.getSensorList(Sensor.TYPE_ORIENTATION);
		if (sensors.size() > 0) {
			sensorManager.registerListener(this, sensors.get(0),
					SensorManager.SENSOR_DELAY_NORMAL);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		WifiManager mWifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
		mWifiManager.disconnect();
		sensorManager.unregisterListener(this);
		NFCReceiveActivity.mClientAgent = null;
		super.onDestroy();
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		if (event.values[0] - nowDeg > 20 || event.values[0] - nowDeg < -20) {
			nowDeg = (int) event.values[0];
			mysufview.setDeg(nowDeg);
			mca.write("senddeg");
			mca.write("" + nowDeg);
		}
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		this.setTitle("BombPlus " + (int) nowDeg);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

		while (flagGamePlaying) {
			// surfaceView繪圖
			mysufview.draw();

			// 假如炸彈被射出螢幕外
			outDeg = mysufview.bOut();
			if (outDeg != -1) {
				Message m = new Message();
				m.what = 0x101;
				m.arg1 = (int) outDeg;
				myHandler.sendMessage(m);
			}
			outDeg = -1;

			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	void ending(boolean win) {

		flagGamePlaying = false;
		flagReadThread = false;
		mysufview.end(win);

		// mca.end();

	}

	@Override
	public void onBackPressed() {
		if (flagGamePlaying) {
			mca.write("gameleft");
		} else {
			mVibrator.cancel();
			if (mMedia != null) {
				mMedia.stop();
				mMedia.reset();
			}

			finish();
			super.onBackPressed();
		}

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		onBackPressed();
		return true;
	}

}
