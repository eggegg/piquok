package lab.BombPlus;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MySurfaceView extends SurfaceView implements
		SurfaceHolder.Callback {

	private SurfaceHolder myholder;
	private Canvas canvas;
	private Bitmap pBitmap[][];
	private Bitmap bBitmap[][];
	private Bitmap backGround;
	private Bitmap ending;
	// 手機朝向的角度
	private float zDeg = 0;
	float scale[] = { 1, 1 };
	int windowSize[] = { 480, 800 };
	// 炸彈飛出的角度,同時用來給GameActivity偵測炸彈的情況
	float outDeg = -1;

	// P:player A:Anime B:Bomb
	private float pLocation[] = { 0, 500 };// x,y
	private int edge = 300;
	private int bombedge = 120;
	private int pANum = 0;// 動畫目前張數
	private int pAKind = 1;// 動畫種類
	private int pSpeed = 30;
	private int pMoveWay = 0;// 0:右 1:左
	private float bLocation[] = { -200, -200 };
	private float bGoto[] = { pLocation[0], pLocation[1] - bombedge };
	private int bANum = 0;
	private int bAKind = 0;
	private int bSpeed = 50;
	private int bMoveWay = -1;// 0:in 1:catched 2:out -1:not in range

	private int cntPANum = 0;
	private int cntPRun = 0;
	private int cntPRunSpeed = 0; // count the time to speed up
	private int cntPStop = 0; // count the time that player have stopped
	private int cntbANum = 0;
	private int cntbCnt = 8;
	// some constants (upper bound)
	private int pSpeedLimit = 50;
	private int pStopTimeLimit = 50;

	private boolean flagMove = true;
	boolean flagHaveBomb = false;
	private boolean flagGamePlaying = true;

	public MySurfaceView(Context context, DisplayMetrics dm) {
		super(context);
		// TODO Auto-generated constructor stub
		myholder = this.getHolder();
		myholder.addCallback(this);
		windowSize[0] = dm.widthPixels;
		windowSize[1] = dm.heightPixels;
		scale[0] = windowSize[0] / 480.f;
		scale[1] = windowSize[1] / 800.f;
		bombedge *= scale[1];
		edge *= scale[0];
		bSpeed *= scale[1];
		pSpeed *= scale[1];
		pLocation[1] *= scale[1];

	}

	// 炸彈飛入
	void bombIn() {
		flagHaveBomb = true;
		bMoveWay = 0;
		flagMove = false;
		bGoto[0] = pLocation[0];
		bGoto[1] = pLocation[1] - bombedge;
		pAKind = 2;
	}

	// 炸彈飛入
	void bombIn(int inDeg) {
		bombIn();
		Log.e("inDeg", "" + inDeg);
		double deg = (inDeg * Math.PI / 180);
		bLocation[0] = (float) (600 * Math.cos(deg) + 240);
		bLocation[1] = (float) (350 - 600 * Math.sin(deg));
		Log.e("outXY", bLocation[0] + "," + bLocation[1]);

	}

	// 炸彈飛出
	private void bombOut() {
		flagHaveBomb = false;
		flagMove = true;
		pAKind = pMoveWay;
		double a = Math.sqrt((double) ((bGoto[0] - bLocation[0])
				* (bGoto[0] - bLocation[0]) + (bGoto[1] - bLocation[1])
				* (bGoto[1] - bLocation[1])));
		double b = bGoto[0] - bLocation[0];

		double c = Math.acos(b / a);
		outDeg = (float) (c * 180 / Math.PI);
		bLocation[0] = -150;
		bLocation[1] = -150;
		pSpeed = (int) (15 * (480.f / 800.f));
		pSpeedLimit *= 480.f / 800.f;
	}

	// 角色往bGoto前進
	public void bombMove() {

		float a = bGoto[0] - bLocation[0];
		float b = bGoto[1] - bLocation[1];
		if ((a != 0 || b != 0)) {
			bLocation[0] = bLocation[0] + (a * bSpeed)
					/ (int) Math.sqrt(a * a + b * b);
			bLocation[1] = bLocation[1] + (b * bSpeed)
					/ (int) Math.sqrt(a * a + b * b);
		}

	}

	// 假如炸彈飛出,回傳角度,不然回傳-1
	float bOut() {
		float t = outDeg;
		outDeg = -1;
		return t;
	}

	// 讓GameActivity設定手機的角度
	void setDeg(float inDeg) {
		zDeg = inDeg;
	}

	void lastTime() {
		bAKind = 1;
		cntbCnt = 2;
	}

	// 繪圖並改變參數
	void draw() {
		try {
			canvas = myholder.lockCanvas();
			canvas.drawColor(Color.BLACK);

			canvas.drawBitmap(backGround, 0, 0, null);
			if (flagHaveBomb) {
				canvas.drawBitmap(bBitmap[bAKind][bANum], bLocation[0],
						bLocation[1], null);
			}
			canvas.drawBitmap(pBitmap[pAKind][pANum], pLocation[0],
					pLocation[1], null);

			// // 印出手機目前的角度
			// Paint p = new Paint();
			// p.setColor(Color.WHITE);
			// p.setTextSize(32);
			// canvas.drawText("" + zDeg, 0, 50, p);

		} catch (Exception ex) {
		} finally {
			if (canvas != null)
				myholder.unlockCanvasAndPost(canvas);
		}

		if (flagHaveBomb) {
			cntbANum++;
			if (cntbANum >= cntbCnt) {
				cntbANum = 0;
				bANum = (bANum + 1) % 4;
			}

			// 炸彈正在飛入
			if (bMoveWay == 0) {
				bombMove();
				// 如果炸彈到定點,則改變bMoveWay
				if (bLocation[1] >= pLocation[1] - bombedge) {
					bLocation[0] = pLocation[0];
					bLocation[1] = pLocation[1] - bombedge;
					bMoveWay = 1;
					flagMove = true;
				}

			}
			// 炸彈正在飛出
			else if (bMoveWay == 2) {
				bombMove();
				if (bLocation[0] < -150 || bLocation[0] > 480
						|| bLocation[1] < -150) {
					bombOut();
				}
			}
			// if the player owns the bomb, then speed up
			cntPRunSpeed++;
			if (cntPRunSpeed >= 3) {
				cntPRunSpeed = 0;
				if (pSpeed < pSpeedLimit)
					pSpeed++;
			}
		}

		// 如果角色處在可移動的狀態
		if (flagMove) {
			cntPANum++;
			if (cntPANum >= 10) {
				cntPANum = 0;
				pANum = (pANum + 1) % 4;
			}

			// 依方向使角色前進
			cntPRun++;
			if (cntPRun >= 3) {
				cntPRun = 0;
				if (pMoveWay == 0) {
					pLocation[0] += pSpeed;
					if (bMoveWay == 1) {
						bLocation[0] += pSpeed;
					}
				} else {
					pLocation[0] -= pSpeed;
					if (bMoveWay == 1) {
						bLocation[0] -= pSpeed;
					}
				}
			}

			// 到達邊界時改變方向
			if (pLocation[0] >= edge) {
				pMoveWay = 1;
				if (!flagHaveBomb) {
					pAKind = 1;
				}
			} else if (pLocation[0] <= 30) {
				pMoveWay = 0;
				if (!flagHaveBomb) {
					pAKind = 0;
				}
			}
		}
		// 不允許長期按住不放
		else {
			if (bMoveWay == 1 && !flagMove) {
				cntPStop++;
				if (cntPStop >= pStopTimeLimit) {
					cntPStop = 0;
					flagMove = true;
				}
			}
		}

	}

	void drawEnd() {

		try {
			canvas = myholder.lockCanvas();
			canvas.drawColor(Color.BLACK);
			canvas.drawBitmap(ending, 0, 0, null);

		} catch (Exception ex) {
		} finally {
			if (canvas != null)
				myholder.unlockCanvasAndPost(canvas);
		}

	}

	// 判斷是否點擊到角色或炸彈
	boolean touchPorB(float inL[]) {
		if ((inL[0] - (pLocation[0] + 50)) * (inL[0] - (pLocation[0] + 50))
				+ (inL[1] - (pLocation[1] + 50))
				* (inL[1] - (pLocation[1] + 50)) < 5000) {

			return true;
		}
		if ((inL[0] - (bLocation[0] + 100)) * (inL[0] - (bLocation[0] + 100))
				+ (inL[1] - (bLocation[1] + 100))
				* (inL[1] - (bLocation[1] + 100)) < 10000) {

			return true;
		}
		return false;
	}

	// 實作TouchEvent事件處理
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!flagGamePlaying) {
			return false;
		}

		float getXY[] = { event.getX(), event.getY() };

		switch (event.getAction()) {

		case MotionEvent.ACTION_DOWN:

			if (touchPorB(getXY)) {
				flagMove = false;
			}

			break;
		case MotionEvent.ACTION_MOVE:
			break;

		case MotionEvent.ACTION_UP:
			cntPStop = 0;
			if (flagHaveBomb && !flagMove) {

				if (bMoveWay == 1) {
					if (!touchPorB(getXY) && getXY[1] < 500) {
						bGoto[0] = (getXY[0] - 75 - bLocation[0]) * 100;
						bGoto[1] = (getXY[1] - 75 - bLocation[1]) * 100;
						bMoveWay = 2;
						flagMove = false;
					} else {
						flagMove = true;
					}

				}
			} else {
				flagMove = true;
			}

			break;
		}

		return true;
	}

	void end(boolean win) {
		flagGamePlaying = false;
		if (win) {
			ending = BitmapFactory.decodeResource(getResources(),
					R.drawable.win);
			
			Matrix m2 = new Matrix();
			m2.postScale(windowSize[0] / (float) ending.getWidth(),
					windowSize[1] / (float) ending.getHeight());
			
			Bitmap e = Bitmap.createBitmap(ending, 0, 0, ending.getWidth(),
					ending.getHeight(), m2, true);
			ending = e;
		}
		drawEnd();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		// Initialized Bitmap classes to import character pictures.

		backGround = BitmapFactory.decodeResource(getResources(),
				R.drawable.background);
		Matrix m = new Matrix();
		m.postScale(windowSize[0] / (float) backGround.getWidth(),
				windowSize[1] / (float) backGround.getHeight());

		Bitmap b = Bitmap.createBitmap(backGround, 0, 0, backGround.getWidth(),
				backGround.getHeight(), m, true);
		backGround = b;
		pBitmap = new Bitmap[3][4];

		pBitmap[0][0] = BitmapFactory.decodeResource(getResources(),
				R.drawable.android_headright1);
		pBitmap[0][1] = BitmapFactory.decodeResource(getResources(),
				R.drawable.android_headright2);
		pBitmap[0][2] = BitmapFactory.decodeResource(getResources(),
				R.drawable.android_headright3);
		pBitmap[0][3] = BitmapFactory.decodeResource(getResources(),
				R.drawable.android_headright2);

		pBitmap[1][0] = BitmapFactory.decodeResource(getResources(),
				R.drawable.android_headleft1);
		pBitmap[1][1] = BitmapFactory.decodeResource(getResources(),
				R.drawable.android_headleft2);
		pBitmap[1][2] = BitmapFactory.decodeResource(getResources(),
				R.drawable.android_headleft3);
		pBitmap[1][3] = BitmapFactory.decodeResource(getResources(),
				R.drawable.android_headleft2);

		// 個人認為1313的效果比1232好
		pBitmap[2][0] = BitmapFactory.decodeResource(getResources(),
				R.drawable.android_with_bomb1);
		pBitmap[2][1] = BitmapFactory.decodeResource(getResources(),
				R.drawable.android_with_bomb3);
		pBitmap[2][2] = BitmapFactory.decodeResource(getResources(),
				R.drawable.android_with_bomb1);
		pBitmap[2][3] = BitmapFactory.decodeResource(getResources(),
				R.drawable.android_with_bomb3);

		bBitmap = new Bitmap[2][4];

		bBitmap[0][0] = BitmapFactory.decodeResource(getResources(),
				R.drawable.black_bomb1);
		bBitmap[0][1] = BitmapFactory.decodeResource(getResources(),
				R.drawable.black_bomb2);
		bBitmap[0][2] = BitmapFactory.decodeResource(getResources(),
				R.drawable.black_bomb3);
		bBitmap[0][3] = BitmapFactory.decodeResource(getResources(),
				R.drawable.black_bomb2);

		bBitmap[1][0] = BitmapFactory.decodeResource(getResources(),
				R.drawable.red_bomb1);
		bBitmap[1][1] = BitmapFactory.decodeResource(getResources(),
				R.drawable.red_bomb2);
		bBitmap[1][2] = BitmapFactory.decodeResource(getResources(),
				R.drawable.red_bomb3);
		bBitmap[1][3] = BitmapFactory.decodeResource(getResources(),
				R.drawable.red_bomb2);

		ending  = BitmapFactory.decodeResource(getResources(), R.drawable.lose);
		
		Matrix m2 = new Matrix();
		m2.postScale(windowSize[0] / (float) ending.getWidth(),
				windowSize[1] / (float) ending.getHeight());
		
		Bitmap e = Bitmap.createBitmap(ending, 0, 0, ending.getWidth(),
				ending.getHeight(), m2, true);
		ending = e;
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
	}

}
