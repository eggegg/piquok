package lab.BombPlus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import lab.BombPlus.R;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.DhcpInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.EventLog.Event;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class BombHostActivity extends Activity {

	private Button btn_start;
	static ServerAgent mServerAgent;
	private NfcAdapter mAdapter;
	private NdefMessage mMessage;
	private String mSSID;
	private String mPW;
	private MediaPlayer mMedia;

	Thread socketListener_t;
	Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.ADD_CLIENT:

				mServerAgent.clientCount++;
				if (mMedia != null) {
					mMedia.stop();
					mMedia.reset();
				}
				mMedia = MediaPlayer.create(BombHostActivity.this, R.raw.bell);

				try {
					mMedia.prepare();
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				mMedia.start();
				Toast.makeText(
						BombHostActivity.this,
						BombHostActivity.this.getResources().getString(
								R.string.players)
								+ (mServerAgent.clientCount + 1),
						Toast.LENGTH_SHORT).show();
				// BombHostActivity.this.setTitle(BombHostActivity.this.getTitle()
				// + " " + (mServerAgent.clientCount + 1));
				break;
			case Global.ADD_INPUT:
				mServerAgent.addInput((BufferedReader) msg.obj);
				break;
			case Global.ADD_OUTPUT:
				mServerAgent.addOutput((PrintWriter) msg.obj);
				break;
			}
		}
	};

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.host);
		mMedia = MediaPlayer.create(BombHostActivity.this, R.raw.bell);
		findViews();
		initVariables();
		setListeners();
		turnOnWifiAP();
		openServerConnection();
		sendNFCMsg();
		BombHostActivity.this.setTitle("BombPlus");
		Log.e(Global.TAG, "DONE");
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		turnOffWifiAP();
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		turnOffWifiAP();
		super.onBackPressed();

	}

	void clearHost() {
		socketListener_t.stop();
		turnOffWifiAP();
	}

	private void sendNFCMsg() {
		try {

			Log.e("nfc", mSSID + " " + mPW);
			byte[] byteSSID = mSSID.getBytes();
			byte[] bytePW = mPW.getBytes();
			mMessage = new NdefMessage(new NdefRecord[] {
					new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
							"text/plain".getBytes(), new byte[] {}, byteSSID),
					new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
							"text/plain".getBytes(), new byte[] {}, bytePW) });
		} catch (java.lang.NoClassDefFoundError e) {
			Toast.makeText(this, "No NFC Device", Toast.LENGTH_LONG).show();
		}
	}

	private void openServerConnection() {
		Log.i(Global.TAG, "+createServer()");
		socketListener_t = new Thread(new SocketListener(mHandler,
				Global.SERVER_PORT));
		socketListener_t.start();
		Log.i(Global.TAG, "-createServer()");
	}

	private void turnOffWifiAP() {
		WifiManager mWifiManager = (WifiManager) getSystemService(WIFI_SERVICE);

		// turn on wifi ap using reflection
		Method[] wmMethods = mWifiManager.getClass().getDeclaredMethods();
		for (Method method : wmMethods) {
			if (method.getName().equals("setWifiApEnabled")) {
				WifiConfiguration netConfig = new WifiConfiguration();
				netConfig.SSID = Global.SERVER_SSID;
				netConfig.allowedKeyManagement
						.set(WifiConfiguration.KeyMgmt.NONE);
				netConfig.allowedAuthAlgorithms
						.set(WifiConfiguration.AuthAlgorithm.OPEN);

				try {
					method.invoke(mWifiManager, netConfig, false);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}

		// close ordinary wifi connection
		if (!mWifiManager.isWifiEnabled()) {
			mWifiManager.setWifiEnabled(true);
		}
	}

	private void turnOnWifiAP() {
		if (mAdapter != null) {
			WifiManager mWifiManager = (WifiManager) getSystemService(WIFI_SERVICE);

			// close ordinary wifi connection
			if (mWifiManager.isWifiEnabled()) {
				mWifiManager.setWifiEnabled(false);
			}

			// turn on wifi ap using reflection
			Method[] wmMethods = mWifiManager.getClass().getDeclaredMethods();
			for (Method method : wmMethods) {
				if (method.getName().equals("setWifiApEnabled")) {
					WifiConfiguration netConfig = new WifiConfiguration();
					netConfig.SSID = Global.SERVER_SSID;
					netConfig.allowedAuthAlgorithms
							.set(WifiConfiguration.AuthAlgorithm.OPEN);
					netConfig.allowedKeyManagement
							.set(WifiConfiguration.KeyMgmt.NONE);

					try {
						method.invoke(mWifiManager, netConfig, true);
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}

			DhcpInfo mDhcpInfo = mWifiManager.getDhcpInfo();
			Log.e(Global.TAG, mDhcpInfo.toString());
		}
	}

	private void setListeners() {
		btn_start.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mServerAgent.writeAll("start");

				Intent intent = new Intent();
				intent.setClass(BombHostActivity.this, ServerGameActivity.class);
				startActivity(intent);
			}
		});

	}

	private void initVariables() {
		try {
			mAdapter = NfcAdapter.getDefaultAdapter(BombHostActivity.this);
		} catch (NoClassDefFoundError e) {
		}
		// Produce the SSID and PW
		mSSID = "BombPlus";// + (int) (Math.random() * 2 + 1);
		mPW = "BombPlus" + (int) (Math.random() * 100 + 1);

		if (mAdapter != null) {
			Toast.makeText(this, R.string.havenfc, Toast.LENGTH_SHORT).show();
		} else {

			Toast.makeText(this, R.string.nonfc, Toast.LENGTH_SHORT).show();
		}
		Global.SERVER_SSID = mSSID;
		// Global.SERVER_KEY = mPW;
		mServerAgent = new ServerAgent();
	}

	private void findViews() {
		btn_start = (Button) findViewById(R.id.start);
	};

	@Override
	public void onResume() {
		super.onResume();
		// push NFC msg.
		try {
			if (mAdapter != null) {
				mAdapter.enableForegroundNdefPush(this, mMessage);
			}

			if (mServerAgent != null) {

				mServerAgent.clear();

			}
		} catch (NoClassDefFoundError e) {
		}
	}

	@Override
	public void onPause() {
		// stop push NFC msg.
		super.onPause();
		try {
			if (mAdapter != null)
				mAdapter.disableForegroundNdefPush(this);
		} catch (NoClassDefFoundError e) {
		}

	}

}
