package lab.BombPlus;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.DhcpInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class BombPlusActivity extends Activity {

	private NfcAdapter mAdapter;
	private NdefMessage mMessage;
	private ProgressDialog dia_join;
	Thread nfc_t;

	private MediaPlayer mMedia;

	Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.START_GAME:
				Intent intent = new Intent();
				intent.setClass(BombPlusActivity.this, ClientGameActivity.class);
				startActivity(intent);
				dia_join.cancel();
				BombPlusActivity.this.finish();
				break;
			case Global.SHOW_MSG:
				dia_join.show();
				break;
			case Global.DISMISS_MSG:
				dia_join.dismiss();
				break;
			case Global.CHANGE_MSG:
				if (mMedia != null) {
					mMedia.stop();
					mMedia.reset();
				}
				mMedia = MediaPlayer.create(BombPlusActivity.this, R.raw.bell);

				try {
					mMedia.prepare();
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				mMedia.start();
				BombPlusActivity.this.getResources().getString(
						R.string.waitforhost);
				dia_join.setTitle(R.string.connectok);
				dia_join.setMessage(BombPlusActivity.this.getResources()
						.getString(R.string.waitforhost));
				break;
			case Global.SOCKET_FAILED:
				dia_join.setTitle(R.string.connectfail);
				dia_join.setMessage(BombPlusActivity.this.getResources()
						.getString(R.string.pleasereconnect));
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Log.e("bug", "comeInBPA " + Global.flagIsPlaying);
		Global.flagIsPlaying = true;
		mMedia = MediaPlayer.create(BombPlusActivity.this, R.raw.bell);
		findViews();
		setListeners();
		nfcRead(true);
		// sendNFCMsg();

	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.e("re", "re");
	}

	private void sendNFCMsg() {
		try {

			mAdapter = NfcAdapter.getDefaultAdapter(BombPlusActivity.this);

			// 消除引號
			String mSSID = Global.SERVER_SSID.substring(1,
					Global.SERVER_SSID.length() - 1);
			String mPW = Global.SERVER_KEY.substring(1,
					Global.SERVER_KEY.length() - 1);

			byte[] byteSSID = mSSID.getBytes();
			byte[] bytePW = mPW.getBytes();
			mMessage = new NdefMessage(new NdefRecord[] {
					new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
							"text/plain".getBytes(), new byte[] {}, byteSSID),
					new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
							"text/plain".getBytes(), new byte[] {}, bytePW) });
		} catch (java.lang.NoClassDefFoundError e) {
			Toast.makeText(this, "No NFC Device", Toast.LENGTH_LONG).show();
		}

	}

	@Override
	protected void onPause() {
		if (mAdapter != null)
			mAdapter.disableForegroundNdefPush(this);
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mAdapter != null) {
			mAdapter.enableForegroundNdefPush(this, mMessage);
		}
		if (NFCReceiveActivity.mClientAgent != null && Global.flagGamePlayed) {
			finish();
		}

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
//		WifiManager mWifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
//		mWifiManager.disconnect();
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Log.e("bp", "bp");
		WifiManager mWifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
		mWifiManager.disconnect();
		super.onBackPressed();
		finish();
	}

	private void setListeners() {

		dia_join.setButton("取消", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				dia_join.dismiss();

				BombPlusActivity.this.finish();

			}
		});
		dia_join.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				BombPlusActivity.this.finish();
			}

		});
	}

	protected void nfcRead(boolean perfromRead) {
		if (perfromRead) {
			nfc_t = new Thread() {
				@Override
				public void run() {

					Message m = mHandler.obtainMessage(Global.SHOW_MSG);
					mHandler.sendMessage(m);

					// nfc result
					try {

						turnOnWifi();

						openClientConnection();

						String st_fromServer = NFCReceiveActivity.mClientAgent
								.read();
						;
						if (st_fromServer.equals("start")) {
							m = mHandler.obtainMessage(Global.START_GAME,
									st_fromServer);
							mHandler.sendMessage(m);
						}

						m = mHandler.obtainMessage(Global.DISMISS_MSG);
						mHandler.sendMessage(m);
					} catch (UnknownHostException e) {
						m = mHandler.obtainMessage(Global.SOCKET_FAILED);
						mHandler.sendMessage(m);
						e.printStackTrace();
					} catch (IOException e) {
						m = mHandler.obtainMessage(Global.SOCKET_FAILED);
						mHandler.sendMessage(m);
						e.printStackTrace();
					}

				}
			};
			nfc_t.start();
		}
	}

	protected void openClientConnection() {
		Log.e(Global.TAG, "+openClientConnection()");

		WifiManager mWifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
		DhcpInfo mDhcpInfo = mWifiManager.getDhcpInfo();

		int ipadd = mDhcpInfo.gateway;
		Global.SERVER_IP = ((ipadd & 0xFF) + "." + (ipadd >> 8 & 0xFF) + "."
				+ (ipadd >> 16 & 0xFF) + "." + (ipadd >> 24 & 0xFF));
		Log.v(Global.TAG, Global.SERVER_IP);

		while (true) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			try {
				if (Global.flagFromNFC) {
					connectToServer();
				}
				mWifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
				mDhcpInfo = mWifiManager.getDhcpInfo();

				ipadd = mDhcpInfo.gateway;
				Global.SERVER_IP = ((ipadd & 0xFF) + "." + (ipadd >> 8 & 0xFF)
						+ "." + (ipadd >> 16 & 0xFF) + "." + (ipadd >> 24 & 0xFF));
				Log.v(Global.TAG, Global.SERVER_IP);
				NFCReceiveActivity.mClientAgent = new ClientAgent(
						Global.SERVER_IP, Global.SERVER_PORT, mHandler);
				break;
			} catch (UnknownHostException e) {
				Toast.makeText(this, "Please connect in Setting",
						Toast.LENGTH_LONG).show();

				finish();
			} catch (IOException e) {
				continue;
			}
		}

		NFCReceiveActivity.mClientAgent.write("");
		Log.e(Global.TAG, "-openClientConnection()");
	}

	private void connectToServer() {
		WifiManager mWifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
		List<WifiConfiguration> mConfigs = mWifiManager.getConfiguredNetworks();

		boolean inConfig = false;
		for (WifiConfiguration config : mConfigs) {
			if (config.SSID.equals(Global.SERVER_SSID)
					|| config.SSID.equals("\"" + Global.SERVER_SSID + "\"")) {
				inConfig = true;
				mWifiManager.enableNetwork(config.networkId, true);
				Log.d(Global.TAG, config.toString());
				break;
			}
		}

		if (!inConfig) {
			Log.v(Global.TAG, "not it config");
			WifiConfiguration netConfig = new WifiConfiguration();
			netConfig.SSID = Global.SERVER_SSID;
			// netConfig.preSharedKey = Global.SERVER_KEY;
			netConfig.allowedAuthAlgorithms
					.set(WifiConfiguration.AuthAlgorithm.OPEN);
			netConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

			if (mWifiManager.enableNetwork(mWifiManager.addNetwork(netConfig),
					true))
				Log.v(Global.TAG, "IP obtain ok");
			else
				Log.v(Global.TAG, "IP obtain failed");
		}
	}

	private void turnOnWifi() {
		Log.e(Global.TAG, "+turnOnWifi()");

		WifiManager mWifiManager = (WifiManager) getSystemService(WIFI_SERVICE);

		// open ordinary wifi connection
		if (mWifiManager.isWifiEnabled()) {
			mWifiManager.setWifiEnabled(false);
		}
		mWifiManager.setWifiEnabled(true);

		Log.e(Global.TAG, "-turnOnWifi()");
	}

	private void findViews() {

		dia_join = new ProgressDialog(BombPlusActivity.this);

		dia_join.setTitle(R.string.connecting);

		dia_join.setMessage(this.getResources().getString(
				R.string.pleaseopenwifi));
	}
}