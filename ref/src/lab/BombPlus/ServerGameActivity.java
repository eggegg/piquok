package lab.BombPlus;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.app.Service;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

public class ServerGameActivity extends Activity implements
		SensorEventListener, Runnable {

	// 因為不能直接在thread中用Toast,所以使用Handler
	Handler myHandler = new Handler() {
		// this method will handle the calls from other threads.
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0x101:// Client將炸彈拋出
				if (flagGamePlaying) {
					int goId = chooseBombGo(msg.arg1, msg.arg2);
					int tmp = msg.arg2;
					tmp = 180 - tmp;
					Log.e("deg", "go to " + goId + "in tmp");
					if (goId == sId) {

						mysufview.bombIn(tmp);

						if (!flagLastTime) {
							mVibrator.vibrate(new long[] { 1000, 1000 }, 0);
							mMedia = MediaPlayer.create(
									ServerGameActivity.this, R.raw.bombslow);
						} else {
							mVibrator.vibrate(new long[] { 500, 500 }, 0);
							mMedia = MediaPlayer.create(
									ServerGameActivity.this, R.raw.bombquick);
						}
						try {
							mMedia.prepare();
						} catch (IllegalStateException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						mMedia.start();
					} else {
						msa.writeToId("bombcome", goId);
						msa.writeToId("" + tmp, goId);
					}
					bombAt = goId;
				}
				break;
			case 0x102:// Client改變角度

				degs[msg.arg1] = msg.arg2;
				break;
			case 0x103:// 炸彈爆炸
				if (mMedia != null) {
					mMedia.stop();
					mMedia.reset();
				}

				if (bombAt == sId) {
					mVibrator.cancel();
					mVibrator.vibrate(new long[] { 0, 2000 }, -1);

					mMedia = MediaPlayer.create(ServerGameActivity.this,
							R.raw.bombblow);

					try {
						mMedia.prepare();
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					mMedia.start();

					msa.writeAll("gamewin");
					Toast.makeText(getBaseContext(), R.string.lose,
							Toast.LENGTH_SHORT).show();
					flagGameWin = false;
					flagGamePlaying = false;

				} else {

					msa.writeToId("gamelose", bombAt);
					msa.writeAll("gamewin");
					Toast.makeText(getBaseContext(), R.string.win,
							Toast.LENGTH_SHORT).show();
					flagGameWin = true;
					flagGamePlaying = false;

				}

				break;

			case 0x106:// 炸彈快爆炸

				msa.writeAll("lasttime");

				if (mMedia != null) {
					mMedia.stop();
					mMedia.reset();
				}
				mMedia = MediaPlayer.create(ServerGameActivity.this,
						R.raw.bombquick);

				if (bombAt == sId) {
					mVibrator.cancel();
					mVibrator.vibrate(new long[] { 500, 500 }, 0);
					try {
						mMedia.prepare();
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					mMedia.start();
				}
				break;
			case 0x107:// 有人離線
				bombAt = msg.arg1;
				this.sendEmptyMessage(0x103);
				break;
			}

		}
	};

	private Vibrator mVibrator;
	private MediaPlayer mMedia;

	Thread[] t;
	Thread gameThread;
	MySurfaceView mysufview;
	private SensorManager sensorManager;
	private float outDeg = -1;
	ServerAgent msa = BombHostActivity.mServerAgent;
	int bombAt;
	int nowDeg;
	int sId;
	int degs[];
	int bombTime = 15;
	int bombLastTime = 10;
	long startTime;
	boolean flagGamePlaying = true;
	boolean flagGameWin = true;
	boolean flagLastTime = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		DisplayMetrics dm = new DisplayMetrics();
		this.getWindowManager().getDefaultDisplay().getMetrics(dm);

		mysufview = new MySurfaceView(this, dm);
		setContentView(mysufview);

		Global.flagGamePlayed = true;

		setBombTime();

		sId = msa.clientCount;
		bombAt = sId;

		degs = new int[sId + 1];
		Log.e("cnt", "" + msa.clientCount);
		t = new Thread[msa.clientCount];
		startTime = System.currentTimeMillis();
		for (int i = 0; i < msa.clientCount; i++) {
			t[i] = new Thread(new ReadClientThread(i, myHandler));
			t[i].start();
		}
		gameThread = new Thread(this);
		gameThread.start();
		mysufview.bombIn();

		mVibrator = (Vibrator) getApplication().getSystemService(
				Service.VIBRATOR_SERVICE);
		mVibrator.vibrate(new long[] { 1000, 1000 }, 0);

		mMedia = MediaPlayer.create(ServerGameActivity.this, R.raw.bombslow);
		try {
			mMedia.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mMedia.start();
	}

	void setBombTime() {
		// bombTime = 20 + sId * 2 + (new Random()).nextInt((sId + 1) * 4);
		// bombLastTime = 5 + (new Random()).nextInt(5);
		bombTime = 120;
		bombLastTime = bombTime - 23;

	}

	@Override
	protected void onResume() {
		super.onResume();
		List<Sensor> sensors = sensorManager
				.getSensorList(Sensor.TYPE_ORIENTATION);
		if (sensors.size() > 0) {
			sensorManager.registerListener(this, sensors.get(0),
					SensorManager.SENSOR_DELAY_NORMAL);
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		BombHostActivity.mServerAgent = new ServerAgent();
		super.onDestroy();
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		if (event.values[0] - nowDeg > 20 || event.values[0] - nowDeg < -20) {
			mysufview.setDeg(event.values[0]);
			degs[sId] = (int) event.values[0];
		}
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
//		this.setTitle("BombPlus " + (int) event.values[0]);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

		while (flagGamePlaying) {
			// surfaceView繪圖
			mysufview.draw();

			// 假如炸彈被射出螢幕外
			outDeg = mysufview.bOut();
			if (outDeg != -1) {

				mVibrator.cancel();
				if (mMedia != null) {
					mMedia.stop();
					mMedia.reset();
				}

				Message m = new Message();
				m.what = 0x101;
				m.arg1 = sId;
				m.arg2 = (int) outDeg;
				myHandler.sendMessage(m);
				outDeg = -1;

			}
			if ((System.currentTimeMillis() - startTime > (bombTime - bombLastTime) * 1000)
					&& !flagLastTime) {
				mysufview.lastTime();
				myHandler.sendEmptyMessage(0x106);
				flagLastTime = true;
			}
			if (System.currentTimeMillis() - startTime > bombTime * 1000) {
				myHandler.sendEmptyMessage(0x103);
			}

			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		mysufview.end(flagGameWin);
		ending();
	}

	int chooseBombGo(int inId, int inDeg) {
		int goId = 0;

		int goDeg = (360 + degs[inId] - 2 * inDeg) % 360;
		int minLDeg = 360;
		int minRDeg = 360;
		int goL = 0;
		int goR = 0;

		for (int i = 0; i < sId + 1; i++) {
			int tl = goDeg - degs[i];
			int tr = degs[i] - goDeg;
			if (tl < 0) {
				tl += 360;
			}
			if (tr < 0) {
				tr += 360;
			}

			if (tl < minLDeg) {

				minLDeg = tl;
				goL = i;
			}
			if (tr < minRDeg) {

				minRDeg = tr;
				goR = i;
			}

		}

		if (goR == inId) {
			goId = goL;
		} else if (goL == inId) {
			goId = goR;
		} else {
			if (minLDeg < minRDeg) {
				goId = goL;
			} else {
				goId = goR;
			}
		}
		return goId;
	}

	void ending() {
	}

	@Override
	public void onBackPressed() {
		if (mMedia != null) {
			mMedia.stop();
			mMedia.reset();
		}
		if (flagGamePlaying) {
			bombAt = sId;
			myHandler.sendEmptyMessage(0x103);
		} else {
			mVibrator.cancel();
			if (mMedia != null) {
				mMedia.stop();
				mMedia.reset();
			}
			super.onBackPressed();
		}

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		onBackPressed();
		return true;
	}

}
