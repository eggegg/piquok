<ISSUE>

#1 server強制hotspot中斷，其他client會FC
   反之有任一clientc中斷連線，server會FC

#2 server等待連線 > back鍵回到桌面 > 再進入等待連線，
   此時連線不應中斷，當sever按下start鍵後，開始遊戲

#3 遊戲在不同手機上的frame rate不一

#4 炸彈有時會丟不見
   -- 當炸彈丟出後，若因為某原因炸彈不在任一人手上，超過一定時間
      server隨機決定一玩家拿炸彈

#5 遊戲途中某Thread吃IOException時，遊戲要繼續嗎
   -- writeAll("start")後，需將產生IOException的Thread對應到的
      output, input移除，playedCount--，關閉此Thread

#6 電子羅盤在遊戲開始前未校正
   -- 感應nfc的同時可以做校正嗎

#7 clients對server放送角度之時機



<RULES>

server-client建立連線後，中斷連線時的結果判定
可以IOException後找尋hotspot來判定
+------------+-------------------+----------+
|            |       server      |  client  |
+------------+-------------------+----------+
| 遊戲開始前 | s:重啟 c:回到桌面 |  c:重連  | 
+------------+-------------------+----------+ 
| 遊戲進行中 |      server輸     | client輸 | 
+------------+-------------------+----------+


